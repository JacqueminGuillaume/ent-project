<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DTW - Digital Tutoring Workspace</title>
    
    
    <base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ request.getContextPath() + "/"%>" />
    

    <!-- Bootstrap Core CSS -->
    <link href="resources/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="resources/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="resources/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="resources/bootstrap/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
   
    
    
</head>

<body>


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h3>DTW - Digital Workspace Tutoring - Bienvenue sur l'Espace Numerique de Travail</h3>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                        	<c:choose>
								<c:when test="${ !isAuth }">
									<a href="authenticate"><i class="fa fa-sign-in"></i>Se connecter</a>
								</c:when>
								<c:otherwise>
									<a href="disconnect"><i class="fa fa-sign-out fa-fw"></i>Se deconnecter</a>
								</c:otherwise>
							</c:choose>
                        </li>
                        
                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    	<li>
                    		<c:if test="${ isAuth }">
                    			Bienvenue ${ sessionScope.firstName }
							</c:if>
                        </li>
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <c:if test="${ isAuth }">
                        	<li>
                        		<c:if test="${sessionScope.isAdmin}">
									<a href="admin/users">Espace Admin</a>
								</c:if>
                        	</li>
                        	<li>
                        		<c:if test="${sessionScope.isProf}">
									<a href="prof/espace-prof">Espace Professeur</a>
								</c:if>
<!--                         		 -->
                        	</li>
                        	<li>
                        		<c:if test="${sessionScope.isStudent}">
									<a href="eleve/espace-eleve">Espace Eleve</a>
								</c:if>
                        	</li>
                        </c:if>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        
        
        
        
        
               <!-- Page Content -->

        
      
      
<div id="page-wrapper" style="background-color: HoneyDew">
	
	
	<div class="container-fluid">
                
       <div id="text-carousel" class="carousel slide" data-ride="carousel" style="width:100%; height:auto; padding:50px">
    <!-- Wrapper for slides -->
    <div class="row">
        <div class="col-xs-offset-3 col-xs-6">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="carousel-content" style="color: black; display:flex; align-item:center;">
                        <div>
                            <h3>Vacances de Toussaint le 01/11/2019</h3>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="carousel-content" style="color: black; display:flex; align-item:center;">
                        <div>
                            <h3>Sejour ski le 05/02/2019</h3>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="carousel-content" style="color: black; display:flex; align-item:center;">
                        <div>                          
                            <h3> Stage mathematiques le 04/12/2019</h3>
                             </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Controls --> <a class="left carousel-control" href="#text-carousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
 <a class="right carousel-control" href="#text-carousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>

</div>

 
</div>




	<div class="container-fluid">
                
        <div class="row">
    		<div class="col-lg-12">
            <a href="authenticate">
      				<img src="resources/bootstrap/img/bg-callout.jpg" style="width:100%; height:auto; padding:50px">
      				<div class="carousel-caption">
             			 <h3>Bienvenue dans votre DTW</h3>
             			 
             		<h3>DWT fournit les fonctionnalites d'un ENT classique et celles d'un EAO revolutionnaire</h3>
            		</div>
    		</a>
			</div>        
		</div>
 
    </div>
            <!-- /.container-fluid -->
</div>      
        
  
  
  <footer class="footer text-center">
      <div class="row">
    	<div class="col-lg-12">
    	<section id="contact" class="map">
      <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
      <br/>
      <small>
        <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
      </small>
    </section>
    
    	</div>
    
    </div>
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="#">
              <i class="icon-social-github"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; Your Website 2018</p>
      </div>
    </footer>      
        

</div>





    <!-- /#wrapper -->

   
    
 
 <script src="resources/bootstrap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="resources/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="resources/bootstrap/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="resources/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="resources/bootstrap/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/bootstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>


</body>

</html>
