<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base
	href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
<meta charset="ISO-8859-1">

<title>login</title>
</head>

<body>
	<h1>Se connecter</h1>
	<hr />
	<h4>
		<a href="">Revenir � la page d'accueil</a>
	</h4>
	<hr />

	<form:form method="post" action="check-login"
		modelAttribute="login-form">

		<form:label path="email">
			<spring:message code="login.lblEmail"></spring:message>
		</form:label>
		<form:input path="email" />
		<br />
		<form:label path="password">
			<spring:message code="login.lblPassword"></spring:message>
		</form:label>
		<form:password path="password" />
		<br />
		<input type="submit" value="Se connecter" />
	</form:form>

</body>
</html>