<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getScheme()+"://"+request.getServerName()+":" 
		+ request.getServerPort()+request.getContextPath()+"/" %>" />
        <title>Eleve</title>
		<meta charset="UTF-8">
</head>
<body>
	<h1>Espace Élève</h1>
	<hr />
	<h4>Bonjour ${ sessionScope.firstName }
		<br />
		<a href="eleve/disconnect">Se déconnecter</a> | 
		<a href="">Revenir à la page d'accueil</a> | 
		<a href="javascript:history.back()">Revenir à la page précédente</a>
	</h4>
	<hr />
	<a href="eleve/quizz" title="passage quizz">Quizz</a> <br />
	<a href="eleve/notes" title="acces notes">Notes</a> <br />
	<a href="eleve/edt" title="edt">Emploi du temps</a> <br />
	
	<nav class="eleveNav">
    <ol>
        <li class="discipline-eleve"><a href="discipline-eleve">Discipline</a></li>
        <li class="eleve/test"><a href="eleve/test">Passer un quizz</a></li>
        <li class="page-upload-download"><a href="page-upload-download">Upload/Download un fichier</a></li>
        <li class="agenda-eleve"><a href="agenda-eleve">Agenda</a></li> 
    
        
    </ol>
</nav>
	
	
</body>
</html>