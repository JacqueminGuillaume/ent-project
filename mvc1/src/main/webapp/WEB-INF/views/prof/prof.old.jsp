<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Bootstrap Theme Company Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
  
  
  <base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
	<link href="<c:url value="resources/css/test.css" />" rel="stylesheet">	
  
</head>







<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
      <a class="navbar-brand" href="#myPage">Espace Professeur</a>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        
        <li><a href="#about">Logout</a></li>
      </ul>
    </div>
  </div>
  
</nav>
    
<div class="sidenav">
 
 	<div class="div-left">
		<form:form method="post" action="prof/search-user" modelAttribute="u">
			<form:input path="name"/>
			<input type="submit" value="rechercher �tudiant" />
		</form:form>
	</div>
	
	<br/>
	
	<div class="div-right">
	
		<div class="upload-btn-wrapper">
		<form:form method="post" action="prof/search-user" modelAttribute="u">
			<button class="btn">Select Quizz file</button>
			<input type="file" name="file" />
			<input type="submit" value="Upload" />
		</form:form>	
		</div>
	
	</div>

<br/>

	<div class="div-right">
	
		<div class="upload-btn-wrapper">
		<form method="POST" action="prof/upload" enctype="multipart/form-data" >
			<button class="btn">Select Eval file</button>
			<input type="file" name="file" />
			<input type="submit" value="Upload" />
		</form>	
		</div>
	
	</div>



</div>
  	

<div class="main" >



</div>


    

 

</body>
</html>
