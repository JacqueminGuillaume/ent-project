<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<meta charset="ISO-8859-1">
<title>Attribution Quizz</title>
</head>
<body>

	<h3>Attribution du Quizz</h3>
	<hr/>
	
<%--     <form:label path="domaine"> Quizz : </form:label></td> --%>
              
<%--     <form:select path="domaine"> --%>
<%-- 			 <form:options value="${quizz}" /> --%>
<%--     </form:select>  --%>
    
    
        <nav style="display: inline-block;">	
   	<fieldset>
		<legend>Quizz :</legend>
		<form:form method="post" action="prof/search-quizz" modelAttribute="q">
			<input type="submit" value="rechercher" />
		</form:form>
	</fieldset>
	
	</nav>
	<hr/>
	
	<c:choose>
		<c:when test="${ resVide }">Aucun resultat</c:when>
		<c:otherwise>
			<table border="1">
				<tr>				
					<th>Domaine</th>
	
				</tr>
				<c:forEach var="q" items="${ quizz }">
					<tr>
						
						<td>${q.domaine  }</td>
	
					</tr>
				</c:forEach>
			</table>
			
		</c:otherwise>
	</c:choose>






</body>
</html>