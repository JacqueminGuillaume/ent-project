<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume - Start Bootstrap Theme</title>

<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ request.getContextPath() + "/"%>" />


    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
    <link href="resources/bootstrap/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="resources/bootstrap/css/resume.min.css" rel="stylesheet">
      
    <link href="resources/bootstrap/css/test.css" rel="stylesheet">  
      
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
  </head>


  <body id="myPage"    data-spy="scroll" data-target=".navbar" data-offset="60">
      
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" style="margin-top:60px"  id="sideNav">
      
      
      
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent" >
        <ul class="navbar-nav">
          
          
         <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="prof/rechercher">Rechercher Etudiant</a>
          </li>
        
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="">Ajout Quiz</a>
          </li>
         
          
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="">Upload Quiz</a>
          </li>
          
          
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="">Ajouter note</a>
          </li>
          
          
          
          
        </ul>
      </div>
    </nav>






        
<nav class="navbar2 navbar-default2 navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#myPage">DWT 2.0</a>
    </div>
    <div class="collapse myposition navbar-collapse">
      <ul class="nav navbar-nav navbar-right mycss" >
        <li><a href="authenticate">Login</a></li>
        
        
      </ul>
    </div>
  </div>
</nav>
      
      
      
      
      
     
      
      
      
      
      
      
      
 <div class="container-fluid p-0 bg-grey">
        
  
     
    <section >
           <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <h4>"Voyage au ski pr�vu le 28 f�vrier"<br><span>pour les �l�ves de 4�me</span></h4>
      </div>
      <div class="item">
        <h4>"Fermeture de l'�cole "<br><span>le 28 juin</span></h4>
      </div>
      <div class="item">
        <h4>"Stage math�matiques "<br><span>pendant les vacances de f�vrier</span></h4>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
        
      </section>

        




     
     

   


  
      
      
      
      
      
      
    <!-- Bootstrap core JavaScript -->
    <script src="resources/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="resources/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="resources/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="resources/bootstrap/js/resume.min.js"></script>
      
      
      
      
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>
      
      
      
      
      
      

  </body>

</html>
