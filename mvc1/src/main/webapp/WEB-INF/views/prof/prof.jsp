<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<!DOCTYPE html>
<html lang="en">

<head>
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title>DTW - Espace Professeur</title>

	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


 <script src="resources/bootstrap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="resources/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="resources/bootstrap/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="resources/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="resources/bootstrap/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/bootstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>



	
	
    <!-- Bootstrap Core CSS -->
    <link href="resources/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="resources/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
	
    <!-- DataTables CSS -->
    <link href="resources/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="resources/bootstrap/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

    
   
    
    
</head>


<body>

	<!-- Gestion de la fonction Ajout quizz  -->

<div id="wrapper">	
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Espace Professeur</a>
            </div>          

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                        	<c:choose>
                        		<c:when test="${ !isAuth }">
                        			<a href="authenticate"><i class="fa fa-sign-in"></i>Se connecter</a>
                        		</c:when>
                        		<c:otherwise>
                        			<li><a href="disconnect"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        		</c:otherwise>
                        	</c:choose>
                        </li>
                    </ul>
                </li>
                
            </ul>
            

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    <li>
                    	Bienvenue ${ sessionScope.firstName }
                    </li>
                        <li class="sidebar-search">
                       
                            <div class="input-group custom-search-form">
                           <form:form method="post" action="prof/search-user" modelAttribute="u">
                           		<form:input path="name" placeholder="rechercher"/>
                                <button class="btn btn-default" type="submit" value="rechercher">
                                    <i class="fa fa-search"></i>
                                </button>
                           	
                            </form:form>

							</div>            
                        </li>

<!--                         <li> -->
<!--                             <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Admin Quizz<span class="fa arrow"></span></a> -->
                            <ul class="nav nav-second-level">
                                <li>
                                    <form:form method="get" action="prof/quizz/add">
										<input type="submit" value="Ajout Quizz" />
									</form:form>
								</li>
                                <li>
                                     <form:form method="get" action="prof/question/add">
										<input type="submit" value="Ajout Question" />
									</form:form>
                                </li>
                                
                                
                                <li>
                                    <a href="uploadquiz">Upload fichier Quiz</a>
                                </li>
                                <li>
                                	<a href="">Revenir a la page d'accueil</a>
                                </li>
								<li>
									<a href="javascript:history.back()">Revenir a la page precedente</a>
								</li>      
                            </ul>
                    </ul>       
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

               <!-- Page Content -->
  
<div id="page-wrapper">
	<div class="container-fluid">
                
        <div class="row">
        
	        <div class="col-lg-12">
	        
					<c:if test="${searchUser eq true }">
						<c:choose>
							<c:when test="${ resVide }">Aucun resultat</c:when>
							<c:otherwise>
 								<%@include file="listeStagiaire.jsp"%>							
							</c:otherwise>
						</c:choose>
					</c:if>

					<c:if test="${addNotes eq true }">
						<%@include file="stagiaireTrouver.jsp"%> 	

						<form:form method="post" action="prof/sendMark"
							modelAttribute="mark">
							<form:input path="value" />
							<input type="hidden" name="id_eleve" value="${user.id}">
							<input type="submit" value="ajout note" />
						</form:form>
					</c:if>

					<c:if test="${addQuizzRequest eq true}">
						<form:form method="post" action="prof/save-quizz" modelAttribute="quizz">
							<form:label path="niveau">Niveau: </form:label>
							<form:input path="niveau" />
							<br />

							<form:label path="domaine">Domaine</form:label>
							<form:input path="domaine" />
							<br />
							
							<form:label path="chapitre">Chapitre</form:label>
							<form:input path="chapitre" />
							<br />


							<form:hidden path="id" />
							<form:hidden path="version" />

							<br />
							<input type="submit" value="creer quizz" />
						</form:form>
					</c:if>

					<c:if test="${ajoutQuestResp eq true}">
						<%@include file="listeQuizz.jsp"%>
					</c:if>
					
					
					<c:if test="${ajoutReponse eq true }">
						<%@include file="quizzTrouver.jsp"%>
						<form:form method="post" action="prof/question/saveInDb" modelAttribute="quizzquestion">


							<form:label path="title">Title:	</form:label>
							<form:input path="title" />
							
							<input type="hidden" name="id_quizz" value="${quizz.id}">
							<input type="submit" value="Ajout Question" />
						</form:form>						
					</c:if>
					
					<c:if test="${showReponse eq true}">
						<%@include file="questionTrouver.jsp"%>
					</c:if>
					
					<c:if test="${writeResponse eq true}">	
						<form:form method="post" action="prof/response/save-response" modelAttribute="reponse"> 
	
	
							<form:label path="response">Proposition: </form:label>
							<form:input path="response" />
							<br />
	
							<form:label path="correct">Status: </form:label>
							<form:checkbox path="correct" />

							<input type="hidden" name="id_question" value="${sessionScope.question.id}">
							<input type="submit" value="ajouter reponse" />
						</form:form>
					</c:if>
					
					<c:if test="${showAttrQuizz eq true }">
	 					<%@include file="stagiaireTrouver.jsp"%>					
						
					<form:form method="post" action="prof/AttrQuizzEleve" modelAttribute="quizz">

					 <th>
					 	<form:label path="domaine">Choississez les quizz a attribuer :</form:label>
					 </th>
        			 <td>
            			<form:input path="domaine" list="listeQuizz" multiple="multiple" />
	                			<datalist id="listeQuizz" >
		                    		<c:forEach var="quizz" items="${listeQuizz}">
		                        		<option value="${quizz.domaine}"/>
		                   			</c:forEach>
	                			</datalist>
       				 </td>
							<input type="hidden" name="idUser" value="${sessionScope.user.id}">
							<input type="submit" value="Attribuer Quizz" />
							
						</form:form>
						
						
	
					</c:if>

			</div>
      			
		</div>     
    		      
	</div>
 
</div>
    
    
   </div>         

</body>

</html>