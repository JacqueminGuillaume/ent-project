<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



    <title>quiztest</title>

<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ request.getContextPath() + "/"%>" />


    <!-- Bootstrap Core CSS -->
    <link href="resources/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="resources/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    
    <!-- Custom CSS -->
    <link href="resources/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
   
    
    
</head>

<body>


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">Quiztest</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                    
                    
                    
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                        <a href="authenticate"><i class="fa fa-sign-in"></i> Login</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                        
                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      
                        
                        
                        
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Admin Quizz<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="prof/quiz/add">Ajouter un Quiz</a>
                                </li>
                                <li>
                                    <a href="?????">Ajouter une question � un quiz</a>
                                </li>
                                
                                <li>
                                    <a href="uploadquiz">Upload fichier Quiz</a>
                                </li>
                                
                                
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.html">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.html"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grid</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Test Quiz<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                
                                <li>
                                    <a href="#">Anglais <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">6eme</a>
                                        </li>
                                        <li>
                                            <a href="quizpublic/anglais/5">5eme</a>
                                        </li>
                                        <li>
                                            <a href="#">4eme</a>
                                        </li>
                                        <li>
                                            <a href="#">3eme</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                
                                
                                <li>
                                    <a href="#">Maths <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">6eme</a>        
                                        </li>
                                        <li>
                                            <a href="">5eme</a>
                                        </li>
                                        <li>
                                            <a href="#">4eme</a>
                                        </li>
                                        <li>
                                            <a href="#">3eme</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        
        
    
     <div id="page-wrapper" style="background-color: grey">
	
          <div class="container-fluid" >      
        <div class="row" style="margin-top: 100px">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        	
                            <p>Quiz:${matiere}</p> 
                            <p>Niveau:${niveau}<p>
                            
                            
                            
                            
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                
                                
                  <!--             
                                <c:forEach var="q" items="${ listeQPublic }">
                                
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
       										<a href="test/public/${ q.id }" title="Select">${q.id}---${ q.chapitre }</a>
                                        </h4>
                                    </div>
                                    
                                </div>
                                
                                
                                </c:forEach>
                                -->   
                                
                                
                            <div class="row">
                                <div class="col-lg-6">
                          	<form:form role="form" method="post" action="home/test-online1/" modelAttribute="cc">
                           
                           		<div class="form-group">
                                      
          								<form:select  id="sel" class="form-control" path="classe" onchange="changed()">
                          			
                          				<form:options items="${classes}"   />
                          			</form:select>
                            	</div>
                         		 
                         		 <div class="form-group">
                                         
        								 <form:select  class="form-control" path="chapitre" id="sel2">
                          			
                          				<form:options id="opt" ></form:options>
                          				
                          			</form:select>
                            	</div>
                         		 
                         		 <form:button type="submit" class="btn btn-default" >Submit</form:button>
                			 </form:form>
                                    
                                    
                                    
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                                
                                
                                
                                
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
 
    </div>
    </div>
    
     
  
  
  
           
        
   
   
 </div>   
 
 <script src="resources/bootstrap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="resources/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

    
    <!-- Custom Theme JavaScript -->
    <script src="resources/bootstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   
<script>


function changed () {
	
	var e = document.getElementById ("sel");
	var classe = e.options [e.selectedIndex] .value;
	console.log("classe click�e: "+classe);
	
	var list = document.getElementById("sel2");
	
	while (list.hasChildNodes()) {   
		  list.removeChild(list.firstChild);
		}
	
	var myOption = document.getElementById("sel2").children.length;
	
	
	
	
	
 <c:forEach var="l" items="${listeAllDomainQZ}">
		var option = document.createElement("option");
		console.log("niveau:"+'${l.niveau}');
		console.log("chapitre"+'${l.chapitre}');
		
		if (  classe == '${l.niveau}'   ) {
		option.value = '${l.chapitre}';
		option.text='${l.chapitre}';
		console.log("option.value:"+option.value);
		list.appendChild(option);
		}
		
		
	</c:forEach>	
		
	
}


</script>








</body>

</html>
