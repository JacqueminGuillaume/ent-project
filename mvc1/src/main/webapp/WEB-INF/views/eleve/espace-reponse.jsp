<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>






<!doctype html>
<html>
<head>

<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
	
	
	<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">		
	
	<link href="<c:url value="/resources/bootstrap/vendor/bootstrap/css/boostrap.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/bootstrap/vendor/bootstrap/css/boostrap.min.css" />" rel="stylesheet">
					
	
	
	
<title>Admin - EXamen</title>
<meta charset="utf-8" />

	

</head>
<body>
	<div class="navbar">
		<a href="" title=""> Bienvenu dans l'espace Tests Quizz</a>
<!--  		<a href="" title="">  </a>
		<a href="" title="">    </a>
-->
 		<a href="admin/quiz/home" title="">Home</a> 
	</div>	


	<br />
	<br />
	
	<p>${test.question }</p>
	<c:set var="question" value = " ${test.question }" />
	
	
	<br />
	<br />


<form:form method="post" action="quizz/test/test-save" modelAttribute="test">
<table border="1">
				<tr>
					
					<th>Réponse 1</th>
					<th>Réponse 2</th>
					
				</tr>
				
				
					<td>
						<div class="item">
							<form:checkbox path="rep1"   />
								<c:out value="${test.reponse1} "></c:out>
							</div>
					
					</td>
				
					<td>
						<div class="item">
							<form:checkbox path="rep2"   />
							<c:out value="${test.reponse2} "></c:out>
						</div>
					</td>
				
					
				
	</table>
	
	
		<form:hidden path="id" />
					
		<input type="submit" value="Suivant" />
					
</form:form>	
				

	
</body>
</html>