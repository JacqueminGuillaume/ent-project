<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title><sitemesh:write property='title'>SpringMVC</sitemesh:write></title>
<meta charset="UTF-8">
</head> 
<body>

	<h1>Student agenda</h1>
	<h4>
		Session ouverte : ${ sessionScope.firstName  }
		<br /> <a href="admin/disconnect">Se déconnecter</a>
	</h4>

	<br />
	
	<hr />

</body>
</html>