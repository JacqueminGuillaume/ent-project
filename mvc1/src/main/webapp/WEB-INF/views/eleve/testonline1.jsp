<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



<title>DTW - Quiz</title>

<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ request.getContextPath() + "/"%>" />


    <!-- Bootstrap Core CSS -->
    <link href="resources/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="resources/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    
    <!-- Custom CSS -->
    <link href="resources/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  
 
 
  
    
</head>

<body>


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h3>Espace El�ve - Passage Quiz</h3>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                        	<c:choose>
								<c:when test="${ !isAuth }">
									<a href="authenticate"><i class="fa fa-sign-in"></i>Se connecter</a>
								</c:when>
								<c:otherwise>
									<li><a href="disconnect"><i class="fa fa-sign-out fa-fw"></i>Se d�connecter</a>
								</c:otherwise>
							</c:choose>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    	<li>
                        	Bienvenue ${ sessionScope.firstName }
                        </li>
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Passer un Quiz<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               
                                <li>
                                    <a href="#">Anglais <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">5eme</a>
                                        </li>
                                        <li>
                                            <a href="#">4eme</a>
                                        </li>
                                        <li>
                                            <a href="#">3eme</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                
                                <li>
                                    <a href="#">Maths <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">5eme</a>
                                        </li>
                                        <li>
                                            <a href="#">4eme</a>
                                        </li>
                                        <li>
                                            <a href="#">3eme</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                
                                
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       <li>
                        	<a href="">Revenir � la page d'accueil</a>
                        </li>
                        <li>
                        	<a href="javascript:history.back()">Revenir � la page pr�c�dente</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        
        
    
     
     
  
  
  <div id="page-wrapper">
	
             
             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            S�lectionner un Quiz
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                          	<form:form role="form" method="post" action="eleve/testonline2/" modelAttribute="mf">
                           
                           		<div class="form-group">
                                      
          							<form:select  id="sel" class="form-control" path="matiere" onchange="changed()">
                          				<form:option value="">--s�lectionner quiz--</form:option>
                          				<form:options items="${listeQZ}"   />
                          			</form:select>
                            	</div>
                         		 
                         		 <div class="form-group">
                                         
        								 <form:select  class="form-control" path="chapitre" id="sel2">
                          			
                          				<form:options id="opt" ></form:options>
                          				
                          			</form:select>
                            	</div>
                         		 
                         		 <form:button type="submit" class="btn btn-default" >Submit</form:button>
                			 </form:form>
                                    
                                    
                                    
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                                
                   
</div>         



   
 </div>   
 
 <script src="resources/bootstrap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="resources/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

    
    <!-- Custom Theme JavaScript -->
    <script src="resources/bootstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   




<script>


function changed () {
	
	var e = document.getElementById ("sel");
	var mat = e.options [e.selectedIndex] .value;
	console.log("matiere click�e: "+mat);
	
	var list = document.getElementById("sel2");
	
	while (list.hasChildNodes()) {   
		  list.removeChild(list.firstChild);
		}
	
	var myOption = document.getElementById("sel2").children.length;
	
	
	
	
	
 <c:forEach var="l" items="${listeAllQZ}">
		var option = document.createElement("option");
		
		if (  mat == '${l.domaine}'   ) {
		option.value = '${l.chapitre}';
		option.text='${l.chapitre}';
		console.log("option.value:"+option.value);
		list.appendChild(option);
		}
		
		
	</c:forEach>	
		
	
}


</script>




</body>

</html>
