<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
					
					
<title><sitemesh:write property='title'>SpringMVC</sitemesh:write></title>


<meta charset="utf-8" />
</head>
<body>
	<h1>Digital Tutoring Workspace</h1>
	<hr />
	<br />
	<a href="authenticate">Formulaire d'authentification</a>
	<br />
	<br />
	<hr />
	<a href="admin/users">Vue Admin</a>
	<br />
	<a href="eleve/espace-eleve">Vue Eleve</a>
	<br />
	<a href="prof/espace-prof">Vue Prof</a>
	<br />
	<a href="espace-quizz"> Espace quizz</a>

	<hr />
	<p>Dev : Jeremy - Mohamed - Ambroise - Guillaume</p>
</body>
</html>