<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>






<!doctype html>
<html>
<head>

<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
	<link href="<c:url value="resources/css/main.css" />" rel="stylesheet">
	<link href="<c:url value="resources/bootstrap/vendor/bootstrap/css/boostrap.css" />" rel="stylesheet">
	<link href="<c:url value="resources/bootstrap/vendor/bootstrap/css/boostrap.min.css" />" rel="stylesheet">
	
	
<title>Liste des Quizz</title>
<meta charset="utf-8" />

	

</head>
<body>
	<h1>Liste des Quizzs</h1>
	
	
	
	<div class="navbar">
	
	<a href="admin/quiz/add" title=""> Ajout Quizz</a>
	
	<a href="admin/question/add" title=""> Ajout Question</a>
	
	<a href="admin/reponse/add" title="">Ajouter Reponse</a>
	
 	<a href="admin/quiz/home" title="">Home</a> 
	
	
	
	</div>	
		
<p> ${classe} </p>
<p> ${matiere} </p>
<p>	${pregunta}</p>



	
	
<div class="main">


<br />

	

		
		
	
	<br />
	<c:if	test = "${addQuizzRequest eq true}" >
			
				<%@include file="quizz-form.jsp" %>

	</c:if>
	
	<c:if	test = "${updateQuizzRequest eq true}" >
			<%@include file="quizz-form-update.jsp" %>
			
	</c:if>
	
	<c:if	test = "${addQuestionRequest eq true}" >
		
				<%@include file="question-form.jsp" %>

	</c:if>

	<c:if	test = "${updateQuestionRequest eq true}" >
		
				<%@include file="response-form.jsp" %>

	</c:if>
	
	<c:if	test = "${addReponseRequest eq true}" >
		
				<%@include file="response-form.jsp" %>

	</c:if>





<c:if	test = "${vuequizz eq true}" >
 
	
	<table border="1">
				<tr>
					
					<th>Quizz Niveau</th>
					<th>Quizz Domaineu</th>
					
				</tr>				
				
		<c:choose>	
		<c:when test="${empty listeQZ}">
		
        		
    	</c:when>
    	<c:otherwise>
    	
    		<c:forEach var="q" items="${ listeQZ }">
					<tr>
						<td>${ q.domaine }</td>
						<td> ${q.niveau }
							
							<td>
							<a href="admin/quiz/select/${ q.id }" title="Select">Select</a>
							<a href="admin/quiz/update/${ q.id }" title="Modifier">Modifier</a>
							<a href="admin/quiz/delete/${ q.id }" title="Supprimer">Supprimer</a>
							<a href="admin/questions/${ q.id }" title="Question">Questions</a>		
					
						</td>
					</tr>
				</c:forEach>
    	</c:otherwise>		
				
	</c:choose>
			
	</table>

</c:if>	








<c:if	test = "${vuequestion eq true}" >
		
				questions:

	
 
	
	<table border="1">
				<tr>
					
					<th>Question Titre</th>
					<th>Question type</th>
					
				</tr>				
				
		<c:choose>	
		<c:when test="${empty listeQS}">
		
        		
    	</c:when>
    	<c:otherwise>
    	
    		<c:forEach var="q" items="${ listeQS }">
					<tr>
						<td>${ q.title }</td>
						<td> ${q.type }
							
							
							<td>
							<a href="admin/question/select/${ q.id }" title="Select">Select</a>
							<a href="admin/question/update/${ q.id }" title="Modifier">Modifier</a>
							<a href="admin/question/delete/${ q.id }" title="Supprimer">Supprimer</a>
							<a href="admin/reponses/${ q.id }" title="Reponses">Reponses</a>		
					
						</td>
					</tr>
				</c:forEach>
    	</c:otherwise>		
				
	</c:choose>
			
	</table>

</c:if>	







<c:if	test = "${vuereponse eq true}" >
		
				reponses

	
 
	
	<table border="1">
				<tr>
					
					<th>Reponse</th>
					<th>Vraie ou Fausse</th>
					
				</tr>				
				
		<c:choose>	
		<c:when test="${empty listeQR}">
		
        		
    	</c:when>
    	<c:otherwise>
    	
    		<c:forEach var="q" items="${ listeQR }">
					<tr>
						<td>${ q.response }</td>
						<td> ${q.correct }
							
							
							<td>
							<a href="admin/reponse/select/${ q.id }" title="Select">Select</a>
							<a href="admin/reponse/update/${ q.id }" title="Modifier">Modifier</a>
							<a href="admin/reponse/delete/${ q.id }" title="Supprimer">Supprimer</a>
								
					
						</td>
					</tr>
				</c:forEach>
    	</c:otherwise>		
				
	</c:choose>
			
	</table>

</c:if>	






















</div>	
	
	
</body>
</html>
