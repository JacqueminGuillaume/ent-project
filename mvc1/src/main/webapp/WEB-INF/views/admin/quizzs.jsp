<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!doctype html>
<html>
<head>

<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title>Admin - Liste des Quizz</title>
<meta charset="utf-8" />



</head>
<body>
	<h1>Admin - Liste des Quizzs</h1>
	<hr />
	<h4>Session ouverte : ${ sessionScopefirstName } 
		<br />
		<a href="admin/disconnect">Se déconnecter</a>
	</h4>
	
	<hr />
	<a href="admin/add-quizz">Ajouter un quizz</a>

	<br />
	

	
		
			<table border="1">
				<tr>
					
					<th>Niveau</th>
					<th>Domaine</th>
				</tr>
				<c:forEach var="q" items="${ quizzs }">
					<tr>
						<td>${ q.niveau }</td>
						<td>${ q.domaine }</td>
						
						<td><a href="admin/quizzs/update/${ q.id }" title="Modifier">Modifier</a>
							<a href="admin/quizzs/delete/${ q.id }" title="Supprimer">Supprimer</a>
							<a href="admin/quizzs/question/${ q.id }" title="Question">Questions</a>		
					
							
							
							
						</td>
					</tr>
				</c:forEach>
			</table>
			<div>
				<c:if test="${ page>1 }">
					<a href="admin/users?page=${ page-1 }&max=${ max }">Précédent</a>
				</c:if>
				<span>${ page }</span>
				<c:if test="${ suivExist }">
					<a href="admin/users?page=${ page+1 }&max=${ max }">Suivant</a>
				</c:if>
			</div>

		
	<br />
	
</body>
</html>
