<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!doctype html>
<html>
<head>
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title>Admin - Reponses</title>
<meta charset="utf-8" />
</head>
<body>
	<h1>Admin - Reponses</h1>
	<hr />
				
	
	
	<form:form method="post" action="admin/response/save-response" modelAttribute="reponse">
		
		
		<form:label path="response">Proposition: </form:label>
		<form:input path="response" />
		<br />
		
		<form:label path="correct">Status: </form:label>
		<form:checkbox path="correct"/>		
		
		
		
		<form:hidden path="id" />
		<form:hidden path="version" />
		

		<input type="submit" value="Sauvegarder-Rep" />
	</form:form>
	
	
	
	
	
	
	
</body>
</html>
