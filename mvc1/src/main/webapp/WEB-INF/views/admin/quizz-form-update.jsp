<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!doctype html>
<html>
<head>
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title>Admin - Update Quiz</title>
<meta charset="utf-8" />
</head>
<body>
	<h1>Admin - Update Quiz</h1>
	
	
	<hr />
	

	<form:form method="post" action="admin/quiz/update" modelAttribute="quizz">
		
		
		<form:label path="domaine">Domaine</form:label>
		<form:input path="domaine" />
		<br />
		
		<form:label path="niveau">Niveau: </form:label>
		<form:input path="niveau" />
		<br />
		
		<form:label path="id">Id: </form:label>
		<form:hidden path="id" />
		
		
		<br />
		
		
		<form:hidden path="version" />
		
		<br />
		
		
		

		<input type="submit" value="Sauvegarder" />
	</form:form>
	
	
	
	
	
	
	
</body>
</html>
