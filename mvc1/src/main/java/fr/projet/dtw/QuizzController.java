package fr.projet.dtw;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.QuizzTest;
import fr.projet.dtw.beans.User;
//import fr.projet.dtw.beans.User.UserType;
import fr.projet.dtw.dao.QuizzDao;
import fr.projet.dtw.dao.QuizzQuestionDao;
import fr.projet.dtw.dao.QuizzResponseDao;
import fr.projet.dtw.dao.QuizzTestDao;
import fr.projet.dtw.dao.UserDao;
import fr.projet.dtw.formbeans.TestForm;


@Controller
public class QuizzController {
	@Autowired
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	//Added by MAB
	@Autowired
	private QuizzDao quizzDao;
	
	@Autowired
	private QuizzQuestionDao questionDao;
	
	
	@Autowired
	private QuizzResponseDao reponseDao;
	
	@Autowired
	private QuizzTestDao testDao;
	
	

	public void setQuizzDao(QuizzDao userDao) {
		this.quizzDao = quizzDao;
	}
	
	
	
	@GetMapping("/espace-quizz")
	public String espaceQuizz() {
		
		
		
		return "/quizz/espace-quizz";
	}
	
	
	
	@GetMapping("/admin/quiz/home")
	public String home () {
		
		
		
		return "/quizz/espace-quizz";
	}
	
	
	@GetMapping("admin/stats")
	public String stats (Model m) {
		
		List<QuizzTest> listeR = new ArrayList<>();
		listeR = null;
		listeR = testDao.findAll2();
		
		
		for (QuizzTest q: listeR) {
			System.out.println(" liste resu: "+q.getQuestion()+ "   "
								+q.getScore()+"  "+q.getCreateTest()+"  "+q.getUser().getFirstName()
								+"  "+q.getQuiz().getDomaine());
			
		}
		m.addAttribute("listeR", listeR);
		return "quizz/index";
		
		
		
		
		
	}
	
	
	
	@GetMapping("/admin/quiz")
	public String adminQuizz1 (Model m, HttpServletRequest request) {
		List<Quizz> listeQZ = new ArrayList<>();
		listeQZ = quizzDao.findAll();
		request.getSession().setAttribute("listeQZ",  listeQZ);
		
		request.getSession().setAttribute("classe", "");
		request.getSession().setAttribute("matiere", "");
		request.getSession().setAttribute("pregunta", "");
		m.addAttribute("vuequizz", true);
		
		return "admin/quiz-vue";
		
		
	}
	
	
	@GetMapping("/admin/quiz/update/{id}")
	public String updateQuizz1 (
			Model m, 
			@PathVariable(name="id", required=false)Long id,
			HttpServletRequest request) {
		
		Quizz quizz = quizzDao.findById2(id);
		
		
		m.addAttribute("quizz", quizz);
		request.getSession().setAttribute("classe", quizz.getNiveau());
		request.getSession().setAttribute("matiere", quizz.getDomaine());
		
		m.addAttribute("updateQuizzRequest", true);
		m.addAttribute("vuequizz", false);
	
		
		//return "admin/quiz-vue";
		return "quizz/page-encours";
		
	}
	
	
	@PostMapping("admin/quiz/update")
	public String updateQuizz2 (@ModelAttribute("q") Quizz q,Model m,
								@RequestParam(name="id", required=false)Long id,
								
								HttpServletRequest request) {
		
		
		//TODO update Quizz dans la db
		
		// Transmission de la nouvelle listeQuizz
		List<Quizz> listeQZ = new ArrayList<>();
		listeQZ = quizzDao.findAll();
		request.getSession().setAttribute("listeQZ",  listeQZ);
		
		
		m.addAttribute("updateQuizzRequest", false);
		m.addAttribute("vuequizz", true);
		
		//return "admin/quiz-vue";
		
		return "quizz/page-encours";
		
	}
	
	
	
	@GetMapping("/admin/quiz/delete/{id}")
	public String deleteQuizz() {
		
		return "/quizz/page-encours";
	}
	
	
	@GetMapping("/admin/quiz/select/{id}")
	public String selectQuizz (Model m, 
								@PathVariable(name="id", required=false)Long id,
								
								HttpServletRequest request) {
		
		Quizz quizz = quizzDao.findById2(id);
		
		
		m.addAttribute("quizz", quizz);
		request.getSession().setAttribute("classe", quizz.getNiveau());
		request.getSession().setAttribute("matiere", quizz.getDomaine());
		
		m.addAttribute("updateQuizzRequest", false);
		m.addAttribute("vuequizz", true);
		
		return "admin/quiz-vue";
		
	}

	
	
	@GetMapping("/admin/question/select/{id}")
	public String selectQuestion (Model m, 
								@PathVariable(name="id", required=false)Long id,
								
								HttpServletRequest request) {

		QuizzQuestion question = questionDao.findById2(id);
		m.addAttribute("question", question);
		
		request.getSession().setAttribute("pregunta", question.getTitle());
		
		
		
		m.addAttribute("updateQuizzRequest", false);
		m.addAttribute("vuequizz", false);
		m.addAttribute("vuequestion", true);
			
		
		return "admin/quiz-vue";
		
	}
	

	
	
	//retourne la liste des questions a partir d'un click sur un quizz
	@GetMapping("/admin/questions/{id}")
	public String listeQuestion (Model m, 
								@PathVariable(name="id", required=false)Long id,
								//@RequestParam(name="listeQS", required=false)Long id,
								HttpServletRequest request) {
		
		//recuperation du quizz de la vue
		Quizz quizz = quizzDao.findById2(id);
		request.getSession().setAttribute("classe", quizz.getNiveau());
		request.getSession().setAttribute("matiere", quizz.getDomaine());
		List<QuizzQuestion> listeQS=null;
		
		listeQS = questionDao.findByQuizzId(quizz);
		request.getSession().setAttribute("listeQS", listeQS);
		
		//TODO si clcik alors changer les variables sessions
		
		//m.addAttribute("updateQuizzRequest", true);
		m.addAttribute("vuequizz", true);
		m.addAttribute("vuequestion",  true);
		m.addAttribute("quizz", quizz);
		
		return "admin/quiz-vue";
		
	}
	
		
	
	
	
	@GetMapping("admin/question/add")
	public String addQuestion (Model m, 
					HttpServletRequest request) {
		
		m.addAttribute("question", new QuizzQuestion());
		m.addAttribute("vuequestion",  true);
		m.addAttribute("addQuestionRequest", true);
		
		return "admin/quiz-vue";
		
	}
	

	
		
		@GetMapping("admin/reponse/add")
		public String addReponse (Model m, 
									HttpServletRequest request) {
			
			
			
			m.addAttribute("reponse", new QuizzResponse());
			//m.addAttribute("vuequizz", true);
			m.addAttribute("vuequestion",  true);
			m.addAttribute("addQuestionRequest", false);
			m.addAttribute("addReponseRequest", true);
			
			
			return "admin/quiz-vue";
			
		}
	
		
		
	
		@PostMapping("admin/response/save-response")
		public String saveReponse (Model m, 
									@RequestParam(name="response", required=false)String response,
									@RequestParam(name="correct", required=false)boolean correct,
									HttpServletRequest request) {
			String laClasse = (String) request.getSession().getAttribute("classe");
			String laMatiere = (String) request.getSession().getAttribute("matiere");
			String laPregunta = (String) request.getSession().getAttribute("pregunta");
			
			
			QuizzQuestion question = questionDao.findByDomaineNiveau2(laPregunta);
			
			QuizzResponse rep = new QuizzResponse ();
			rep.setCorrect(correct);
			rep.setResponse(response);
			rep.setQrep(question);
		
			reponseDao.insert(rep);
			m.addAttribute("vuequestion",  true);
			m.addAttribute("addQuestionRequest", false);
			
			return "admin/quiz-vue";
			
		}
		
		
		//Reponse au foormulaire de creation d'un question
		@PostMapping("admin/question/save")
		public String addQuestion2 (Model m, 
									
									@RequestParam(name="title", required=false)String title,
									@RequestParam(name="type", required=false)String type,
									HttpServletRequest request) {
			
			String laClasse = (String) request.getSession().getAttribute("classe");
			String laMatiere = (String) request.getSession().getAttribute("matiere");
			
			Quizz quizz = quizzDao.findByDomaineNiveau2(laMatiere, laClasse);
			
			
			QuizzQuestion QS = new QuizzQuestion ();
			QS.setTitle(title);
		//	QS.setType(type);
		//	QS.setQuiz_quizquestion(quizz);  //modified 3 janvier 2:55
			QS.setQuizq(quizz);
			
			questionDao.insert(QS);
			quizzDao.update(quizz);
			
			
			m.addAttribute("question", new QuizzQuestion());
			
			
			List<QuizzQuestion> listeQS=null;
			
			
			listeQS = questionDao.findByQuizzId(quizz);
			
			//TODO Mise à jour des variables suite à modification
			
			request.getSession().setAttribute("listeQS", listeQS);
			
			request.getSession().setAttribute("pregunta", title);
			//m.addAttribute("vuequizz", true);
			m.addAttribute("vuequestion",  true);
			m.addAttribute("addQuestionRequest", false);
			
			return "admin/quiz-vue";
			
		}
		
	
	//AJout QUIZZ a partir du menu
	
	@GetMapping("prof/quiz/add")
	public String addQuizz1 (Model m, 
								
								HttpServletRequest request) {
		
		Quizz quizz = new Quizz(); User u = new User();
		
		System.out.println("demande ajout quiz");
		m.addAttribute("quizz", quizz);
		m.addAttribute("u", u);
		//m.addAttribute("addQuizzRequest", true);
		//m.addAttribute("vuequizz", false);
		//m.addAttribute("vuequestion",  false);
		
		//return "admi/quiz-vue";
		return "prof/add-quiz";
	}
	
	
	@PostMapping("prof/quiz/save-quizz")
	public String addQuizz2 (@ModelAttribute("q") Quizz q,Model m,
								@RequestParam(name="id", required=false)Long id,
								
								HttpServletRequest request) {
		
		
		System.out.println("quiz reçu:"+q.getChapitre()+"  "+q.getDomaine()+"  "+q.getNiveau());
		//TODO update Quizz dans la db
		Quizz quizz = new Quizz();
		
		quizz.setNiveau(q.getNiveau());
		quizz.setDomaine(q.getDomaine());
		quizz.setChapitre(q.getChapitre());
		quizzDao.insert(quizz);
		
	/*	
		List<Quizz> listeQZ = new ArrayList<>();
		
		listeQZ = quizzDao.findAll();
		
		request.getSession().setAttribute("listeQZ",  listeQZ);
		
		m.addAttribute("listQZ", listeQZ);
		m.addAttribute("updateQuizzRequest", false);
		m.addAttribute("vuequizz", true);
		m.addAttribute("vuequestion",  false);
		
		return "admin/quiz-vue";  */
		
		User u = new User();
		m.addAttribute("u",  u);
		
		return "prof/prof";
	}
	
	
	
	//retourne la liste des questions a partir d'un click sur un quizz
		@GetMapping("/admin/reponses/{id}")
		public String listeReponses (Model m, 
									@PathVariable(name="id", required=false)Long id,
									//@RequestParam(name="listeQS", required=false)Long id,
									HttpServletRequest request) {
			
			QuizzQuestion question = questionDao.findById2(id);
			List<QuizzResponse> listeQR=null;
		
			listeQR = reponseDao.findByQuestion(question);
			m.addAttribute("vuequizz", true);
			m.addAttribute("vuequestion",  true);
			m.addAttribute("vuereponse", true);
			m.addAttribute("listeQR",  listeQR);
			
			return "admin/quiz-vue";
			
		}
	
	
		
		//Gestion des test
		
		@GetMapping("/eleve/test")
		public String adminTest (Model m, HttpServletRequest request) {
			
			 
			
			List<Quizz> listeQZ = quizzDao.findAll();
			String email = (String) request.getSession().getAttribute("email");
			
			
			
			
			System.out.println("user connecté: "+ email); 
			User user = userDao.findByEmail(email);			
			m.addAttribute("listeQZ",  listeQZ);
			
			return "eleve/espace-test";
			
		}
	
	
		
		@GetMapping("/test/quiz/select/{id}")
		public String adminTest2 (Model m, @PathVariable(name="id", required=false)Long id,
				HttpServletRequest request, HttpServletResponse response) {
		
			
			
			
			Quizz qz = quizzDao.findById2(id);
			List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(qz);
			HttpSession session = request.getSession();
			session.setAttribute("listeQuestions", listeQuestions);
			session.setAttribute("quizztest", qz);
			List<QuizzResponse>  listeReponses = listeQuestions.get(0).getReponses();
			String rep1 = listeReponses.get(0).getResponse();
			String rep2=  listeReponses.get(1).getResponse();
			TestForm test = new TestForm();
			test.setQuestion(listeQuestions.get(0).getTitle());
			test.setReponse1(rep1);
			test.setReponse2(rep2);
			test.setRep1(false);
			test.setRep2(false);
			m.addAttribute("test",  test);
			return "eleve/espace-reponse"; 
			
			
			
		}
		
		@PostMapping("quizz/test/test-save")
		public String saveTest (@ModelAttribute("test") TestForm t,Model m,
									@RequestParam(name="question", required=false)String qst,
									@RequestParam(name="rep1", required=false) boolean rep1,
									@RequestParam(name="rep2", required=false) boolean rep2,
									
									HttpServletRequest request) {
 			
			HttpSession session = request.getSession(true);
			User user =  (User) request.getSession().getAttribute("user");
			Quizz qztest = (Quizz) request.getSession().getAttribute("quizztest");
			List<QuizzQuestion>   listeQuestions =  (List<QuizzQuestion>) request.getSession().getAttribute("listeQuestions");
	
			
			//Test si bonne réponse
			List<QuizzResponse> listeReponses = listeQuestions.get(0).getReponses();
			Boolean sol1 = listeReponses.get(0).isCorrect();Boolean sol2=  listeReponses.get(1).isCorrect();
			
			int score = 0;
			if (sol1==rep1 && sol2==rep2) {
				System.out.println("Bonne réponse pour "+qst); score += 1;
			}
			else System.out.println("Réponse fausse");
			
			// creation de l'objet dans la DB
			QuizzTest qt = new QuizzTest();
			qt.setQuestion(listeQuestions.get(0).getTitle());
			qt.setReponse1(listeReponses.get(0).getResponse());qt.setReponse2(listeReponses.get(1).getResponse());
			qt.setSolution1(sol1);qt.setSolution2(sol2);qt.setQuiz(qztest);qt.setUser(user);qt.setScore(score);
			LocalDateTime dt = LocalDateTime.now();
			qt.setCreateTest(dt);
			
			
			testDao.insert(qt);
			
			
			//suppression question en tete
			listeQuestions.remove(0);
			
			for (QuizzQuestion q: listeQuestions) {
				System.out.println("question restante apres le remove"+ q.getTitle());
			}
			
			session.setAttribute("listeQuestions", listeQuestions);
			if ( ! listeQuestions.isEmpty()) {
			
			//affichage question suivante et ses réponses
			String textQuestion = listeQuestions.get(0).getTitle();
			listeReponses = listeQuestions.get(0).getReponses();
			
			
			String isReponse1 = listeReponses.get(0).getResponse();
			String isReponse2 = listeReponses.get(1).getResponse();
			System.out.println("boolean:   "+isReponse1+"   "+isReponse2);
			
			
			TestForm test = new TestForm();
			test.setReponse1(isReponse1);
			test.setReponse2(isReponse2);
			test.setQuestion(textQuestion);
			
			
			
			
			m.addAttribute("test", test);
			return "eleve/espace-reponse";
			
			}
			else {
				System.out.println("Fin Quizz  à bientot");
				
				return "quizz/fin-quiz";
				//return "quizz/espace-quizz";
				}
				
				 
			
			}
			
			
}
		
		
	



	
	
	
	