package fr.projet.dtw;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.dao.QuizzDao;
import fr.projet.dtw.dao.QuizzQuestionDao;
import fr.projet.dtw.dao.QuizzResponseDao;
import fr.projet.dtw.formbeans.LoginForm;
import fr.projet.dtw.formbeans.QuizzForm;
import fr.projet.dtw.formbeans.TestForm;
import fr.projet.dtw.tools.Tools;


@Controller
public class UploadController {
	
	
	@Autowired
	private QuizzDao quizzDao;
	
	
	@Autowired
	private QuizzQuestionDao questionDao;
	
	@Autowired
	private QuizzResponseDao reponseDao;
	
	
	@GetMapping("/uploadquiz")
	public String showLogin(Model model) {
		
		System.out.println("ds uploadquiz controller");
		
		return "/prof/prof-uploadquiz"; 
	}
	
	
	
	@PostMapping("/prof/upload")
	//@RequestMapping("/prof/upload")
    public String uploadCsv(Model model,
                    HttpServletRequest request,
                    @RequestParam("file") MultipartFile file) {
		
		
		
            if(!file.isEmpty()) {
                    try {
                    	
                            byte[] contentBytes = file.getBytes();
                            String dirPath = "/users/anzal/desktop/uploads";
                            //String dirPath = request.getServletContext().getRealPath("")+"/uploads";  //en mode production
                            File dir = new File(dirPath);
                            
                            
                            if(!dir.exists())
                                    dir.mkdirs();
                            
                            String filePath = dir.getAbsolutePath()+File.separator+file.getOriginalFilename();
                            
                            
                            
                            try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath)))
                            	{
                                    bos.write(contentBytes);
                            	}
                            
                            	List<QuizzForm> myImportedList = Tools.importQuizzCsv(filePath);
                            	
                            
                            	for (QuizzForm x: myImportedList ) {
                            		//TODO check if user exists
                            		
                            		
                            		
                            		Quizz quizz = new Quizz();
                            		quizz.setNiveau(x.getNiveau());
                            		quizz.setDomaine(x.getDomaine());
                            		quizz.setChapitre(x.getChapitre());
                            		
              Long id = quizzDao.findId(x.getNiveau(),  x.getDomaine(),  x.getChapitre());
                       		
              if (id==null) {           		
            	  
            	  quizzDao.insert(quizz);
            	  id = quizzDao.findId(x.getNiveau(),  x.getDomaine(),  x.getChapitre());
            	  
            	  
            	  
            	  Quizz quizn = quizzDao.findById2(id);
            	  
            	  
            	  QuizzQuestion question = new QuizzQuestion();
          		//TO COMPLETE
          		question.setTitle(x.getTitle());
          		question.setQuizq(quizn);
          		Long idq= questionDao.insert(question);
      			quizzDao.update(quizn);
      			
      			///chercher la question ajoutee
      			QuizzQuestion questionn = questionDao.findById2(idq);
      			
      			QuizzResponse reponse= new QuizzResponse();
    			reponse.setResponse(x.getCorrect());
    			reponse.setCorrect(true);
    			reponse.setQrep(questionn);
    			
    			reponseDao.insert(reponse);
    			
    			QuizzResponse reponse2 = new QuizzResponse();
    			reponse2.setResponse(x.getReponse());
    			reponse2.setCorrect(false);
    			reponse2.setQrep(questionn);
    			reponseDao.insert(reponse2);
      			
      			
            	  
            	  
              }
             
              else {
            	  Quizz quizn = quizzDao.findById2(id);
            	  QuizzQuestion question = new QuizzQuestion();
          		//TO COMPLETE
          		question.setTitle(x.getTitle());
          		question.setQuizq(quizn);
          		Long idq = questionDao.insert(question);
      			quizzDao.update(quizn);
      			
      			QuizzQuestion questionn = questionDao.findById2(idq);
      			
      			QuizzResponse reponse= new QuizzResponse();
    			reponse.setResponse(x.getCorrect());
    			reponse.setCorrect(true);
    			reponse.setQrep(questionn);
    			
    			reponseDao.insert(reponse);
    			
    			QuizzResponse reponse2 = new QuizzResponse();
    			reponse2.setResponse(x.getReponse());
    			reponse2.setCorrect(false);
    			reponse2.setQrep(questionn);
    			reponseDao.insert(reponse2);
      			
      			
      			
      			
      			
            	  
              }
            	 
                         /*   		
                            		
                            		
                        			
                        			
                        			System.out.println("ligne 1 inserted");
                            	*/	
                            		
                            	}
                            	
                            
                    	} catch (Exception e) {
                            e.printStackTrace();
                    	}        
            
            }   
            User u = new User();
            model.addAttribute("u",  u);
    //        return "/prof/prof";  
		return "hi";
    
	                        	
	}
}


