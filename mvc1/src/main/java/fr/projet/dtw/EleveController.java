package fr.projet.dtw;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.projet.dtw.beans.DevoirMaison;
import fr.projet.dtw.beans.MonImage;
import fr.projet.dtw.beans.TrainingCourse;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.dao.DevoirMaisonDAO;
import fr.projet.dtw.dao.ImageDao;
import fr.projet.dtw.dao.TrainingCourseDao;
import fr.projet.dtw.dao.UserDao;
 
@Controller
public class EleveController {

	@Autowired
	private ImageDao imageDao;

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private DevoirMaisonDAO devoirMaisonDAO;

	@Autowired
	private TrainingCourseDao trainingCourseDao;

	@Autowired
	private TrainingCourseDao trainingcourse;

	String sb = new String();

	@GetMapping("/eleve/espace-eleve")
	public String eleve() {
		return "eleve/eleve";
	}

	@GetMapping("/eleve/quizz")
	public String planningEleve() {
		return "eleve/quizz";
	}

	@GetMapping("/eleve/notes")
	public String notesEleve() {
		return "eleve/notes";
	}

	@GetMapping("/page-upload-download")
	public String uploadDownloadFichier() {
		return "eleve/upload-download";
	}
	
	@GetMapping("/eleve/edt")
	public String edtEleve() {
		return "eleve/edt";
	}
	
	@GetMapping("/page-upload")
	public String uploadFichier() {
		return "eleve/upload";
	}

	@PostMapping("/eleve/upload")
	public String versDB(HttpServletRequest request) {
		String chemin = request.getParameter("chemin");
		File folder = new File(chemin);
		MonImage images = null;
		try {
			images = MonImage.serializeFolder(folder);
		} catch (IOException e) {
			e.printStackTrace();
		}
		imageDao.insert(images);
		return "eleve/eleve"; 
	}  

	@PostMapping("/eleve/download")
	public String versORDI(HttpServletRequest request) {

		
		String chemindownload = request.getParameter("chemindownload");
		File folder2 = new File(chemindownload);
		MonImage image = imageDao.findById(7);

		if (!folder2.exists()) {
			folder2.mkdir();
		}

		try {
			MonImage.writeFilesIntoFolder(image, folder2);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "eleve/eleve";
	}

	@GetMapping("/insert-course")
	public String insertCourse() {

		for (int i = 0; i < 5; i++) {
			TrainingCourse tc = new TrainingCourse(null, "math" + i, userDao.findAll());
			trainingCourseDao.insert(tc);
			trainingCourseDao.getHibernateTemplate().evict(tc);
		}
		return "home";
	}

	@GetMapping("/course-etudiant")
	public String ajoutCourseEtudiant() {
		User u = userDao.findById(2);
		List<TrainingCourse> lt = u.getCourses();

		lt.add(trainingCourseDao.findById(3)); 
		userDao.update(u);
		return "home";
	}
	
	@GetMapping("/devoir-maison")
	public String ajoutDmEtudiant(HttpServletRequest request) {
		User u = userDao.findById(2);
		List<DevoirMaison> ldevoir = null;

		ldevoir.add(devoirMaisonDAO.findById(1));
		userDao.update(u);
		return "home";
	}

	@GetMapping("/discipline-eleve")
	public String eleveCours(Model m, HttpServletRequest request) {
		List<TrainingCourse> listeTC = new ArrayList<>();
		listeTC.addAll(trainingcourse.findByUser((Long) request.getSession().getAttribute("user_id")));
		m.addAttribute("disciplines", listeTC); // que pour la page JSP
		return "eleve/discipline";
	}

//	@GetMapping("/image-vers-DB")
//	public String versDB() {
//		File folder = new File("C:\\Users\\Admin stagiaire\\Desktop\\image");
//		MonImage images = null;
//
//		try {
//			images = MonImage.serializeFolder(folder);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		imageDao.insert(images);
//		return "home";
//	}

//	@GetMapping("/image-vers-ORDI")
//	public String versORDI() {
//
//		File folder2 = new File("C:\\Users\\Admin stagiaire\\Desktop\\image2");
//		MonImage image = imageDao.findById(1);
//
//		if (!folder2.exists()) {
//			folder2.mkdir();
//		}
//
//		try {
//			MonImage.writeFilesIntoFolder(image, folder2);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return "home";
//	}

}