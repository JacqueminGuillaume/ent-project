package fr.projet.dtw;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.projet.dtw.beans.User;
import fr.projet.dtw.beans.User.UserType;
import fr.projet.dtw.dao.UserDao;
import fr.projet.dtw.formbeans.LoginForm;

@Controller
public class LoginController {
	
	@Autowired
	private UserDao userdao;
	
	
	@GetMapping("/authenticate")
	public String showLogin(Model model) {
		LoginForm f = new LoginForm("professor@dawan.fr","professor");
		model.addAttribute("login-form", f);
		return "login"; 
	}
	
	
	@PostMapping("/check-login")
	public String checkLogin(
			HttpServletRequest request,
			Model model, 
			@Valid @ModelAttribute("login-form") LoginForm form, 
			@ModelAttribute("u") User u,
			BindingResult result) {
		String cible = "";
		if (result.hasErrors()) {
			model.addAttribute("errors", result);
			model.addAttribute("login-form",form);
			return "login";
		}
		
		
		u = userdao.findByEmail(form.getEmail());
		if(u!= null && u.getPassword().equals(form.getPassword())) {
			if (u.getAccountType().equals(UserType.ADMIN)) {
				model.addAttribute("adminInitialise", true);
				cible ="/admin/users";
			}else if (u.getAccountType().equals(UserType.PROFESSOR)) {
				model.addAttribute("initialise", true);
				cible ="/prof/prof";
			}else if (u.getAccountType().equals(UserType.STUDENT)) {
				
				cible ="/eleve/eleve";
			}
			request.getSession().setAttribute("email", form.getEmail()); 
			request.getSession().setAttribute("user_id", u.getId());
			request.getSession().setAttribute("firstName", u.getFirstName());
			request.getSession().setAttribute("lastname", u.getName());
			request.getSession().setAttribute("isAuth", true);
			request.getSession().setAttribute("isStudent", u.isEtudiant());
			request.getSession().setAttribute("isProf", u.isProfesseur());
			request.getSession().setAttribute("isAdmin", u.isAdmin());
			request.getSession().setAttribute("role", u.getAccountType());
		}else {
			model.addAttribute("msg", "Erreur d'authentification !");
			model.addAttribute("login-form",form);
			cible="login";
		}
		
		System.out.println("cible = "+cible);
		return cible; 
		
	}

}
