package fr.projet.dtw.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Pattern;


@Entity
@Table(name="t_users")
public class User implements Serializable{

	@Id 
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(length=150, nullable=false)
	private String name;

	@Column(length=150, nullable=false)
	private String firstName;

	@Column(nullable=false, unique=true) 
	@Pattern(regexp="\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message="Email invalide")
	private String email;


	public enum UserType {
		STUDENT, ADMIN, PROFESSOR, STUDENT_PARENT
	}

	@Enumerated(EnumType.STRING)
	private UserType accountType;
	
		

	@ManyToMany(cascade=javax.persistence.CascadeType.ALL, fetch=FetchType.EAGER)
	private List<TrainingCourse> courses;
	
	@OneToMany(mappedBy="user", cascade=javax.persistence.CascadeType.ALL)
	private List<QuizzTest> quizTests;
	
	
	public List<QuizzTest> getQuizTests() {
		return quizTests;
	}

	public void setQuizTests(List<QuizzTest> quizTests) {
		this.quizTests = quizTests;
	}

	public List<Quizz> getQuiz_user() {
		return quiz_user;
	}

	public void setQuiz_user(List<Quizz> quiz_user) {
		this.quiz_user = quiz_user;
	}

	public List<DevoirMaison> getDevoir_maison() {
		return devoir_maison;
	}

	public void setDevoir_maison(List<DevoirMaison> devoir_maison) {
		this.devoir_maison = devoir_maison;
	}

	@ManyToMany(cascade=javax.persistence.CascadeType.ALL)
	private List<Quizz> quiz_user;
	
	@ManyToMany(cascade=javax.persistence.CascadeType.ALL)
	private List<DevoirMaison> devoir_maison;
	
	
	private String password;

	@Version
	private int version;

	private boolean absent;

	public User() {
		super();
	}

	public User(Long id, String name, String firstName,
			@Pattern(regexp = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message = "Email invalide") String email,
			UserType accountType, List<TrainingCourse> courses, String password) {
		super();
		this.id = id;
		this.name = name;
		this.firstName = firstName;
		this.email = email;
		this.accountType = accountType;
		this.courses = courses;
		this.password = password;
	}

	public User(Long id, String name, String firstName,
			@Pattern(regexp = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message = "Email invalide") String email,
			UserType accountType) {
		super();
		this.id = id;
		this.name = name;
		this.firstName = firstName;
		this.email = email;
		this.accountType = accountType;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserType getAccountType() {
		return accountType;
	}

	public void setAccountType(UserType accountType) {
		this.accountType = accountType;
	}

	public List<TrainingCourse> getCourses() {
		return courses;
	}

	public void setCourses(List<TrainingCourse> courses) {
		this.courses = courses;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public boolean isAbsent() {
		return absent;
	}

	public void setAbsent(boolean absent) {
		this.absent = absent;
	}

	public boolean isEtudiant() {
		return accountType.equals(UserType.STUDENT);
	}

	public boolean isProfesseur() {
		return accountType.equals(UserType.PROFESSOR);
	}
	
	public boolean isAdmin() {
		return accountType.equals(UserType.ADMIN);
	}
}