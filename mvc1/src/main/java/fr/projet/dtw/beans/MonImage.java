package fr.projet.dtw.beans;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "images")
public class MonImage implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;

	private String name;

	@Lob()
	private byte[] image;

	@Column(name = "mime_type")
	private String mimeType;

	@Version
	private int version;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public static Map<String, String> getAcceptedmimetype() {
		return acceptedMimeType;
	}

	public MonImage(Long id, String name, byte[] image, String mimeType, int version) {
		super();
		Id = id;
		this.name = name;
		this.image = image;
		this.mimeType = mimeType;
		this.version = version;
	}

	public MonImage() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		result = prime * result + Arrays.hashCode(image);
		result = prime * result + ((mimeType == null) ? 0 : mimeType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + version;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonImage other = (MonImage) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		if (!Arrays.equals(image, other.image))
			return false;
		if (mimeType == null) {
			if (other.mimeType != null)
				return false;
		} else if (!mimeType.equals(other.mimeType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	public void setImage(BufferedImage bufferedImage) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, mimeType.split("image/")[1], baos);
		baos.flush();
		this.image = baos.toByteArray();
		baos.close();
	}

	public BufferedImage getBufferedImage() throws IOException {
		return ImageIO.read(new ByteArrayInputStream(image));
	}

	transient public static final Map<String, String> acceptedMimeType = new HashMap<String, String>();
	static {
		acceptedMimeType.put("jpg", "image/jpeg");
		acceptedMimeType.put("gif", "image/gif");
		acceptedMimeType.put("png", "image/png");
		acceptedMimeType.put("tif", "image/tiff");
		acceptedMimeType.put("svg", "image/svg+xml");
		acceptedMimeType.put("ico", "image/vnd.microsoft.icon");
	}

	public static MonImage serializeImage(File imageFile) throws IOException {
		MonImage monImage = new MonImage();
		monImage.setName(imageFile.getName().substring(0, imageFile.getName().lastIndexOf(".")));
		monImage.setMimeType((String) MonImage.acceptedMimeType
				.get(imageFile.getName().substring(imageFile.getName().lastIndexOf(".") + 1)));
		monImage.setImage(ImageIO.read(imageFile));
		return monImage;
	}

	public static void writeFileIntoFolder(MonImage monImage, File folder) throws IOException {
		String extension = getExtension(monImage.getMimeType());
		File file = new File(folder + "/" + monImage.getName() + "." + extension);
		file.createNewFile();
		FileImageOutputStream fos = new FileImageOutputStream(file);
		fos.write(monImage.getImage());
		fos.close();
	}

	/**
	 * RÃ©cupÃ¨re l'extension du fichier en fonction du type mime dans le
	 * dictionnaire des formats d'image acceptÃ©s
	 * 
	 * @param mimeType
	 * @return
	 */
	private static String getExtension(String mimeType) {
		for (Entry<String, String> typeMimeEntry : MonImage.acceptedMimeType.entrySet()) {
			if (mimeType.equals(typeMimeEntry.getValue())) {
				return (String) typeMimeEntry.getKey();
			}
		}
		return null;
	}

	public static void writeFilesIntoFolder(MonImage image, File folder) throws IOException {
		writeFileIntoFolder(image, folder);
	}

	public static MonImage serializeFolder(File folder) throws IOException {
		MonImage images = null;
		// Filtre pour ne garder que les fichiers images acceptés
		FileFilter imageFileFilter = new FileFilter() {
			@Override
			public boolean accept(File file) {
				String fileName = file.getName();
				// on extrait l'extension du fichier puis on vérifie qu'elle existe dans le
				// dictionnaire
				String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
				if (extension == null || extension.isEmpty())
					return false;
				else
					return MonImage.acceptedMimeType.containsKey(extension);
			}
		};
		for (File imageFile : folder.listFiles(imageFileFilter)) {
			MonImage image = serializeImage(imageFile);
			if (image != null) {
				images = image;
			}
		}
		return images;
	}

}
