package fr.projet.dtw.beans;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="t_markt")
public class TestMark {
	
	
	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne()
	private Quizz quizn;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private User usern;
	

	@Column()
	private LocalDateTime date;
	

	@Column()
	private int score;
	

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	
	
	public User getUsern() {
		return usern;
	}

	public void setUsern(User usern) {
		this.usern = usern;
	}

	public Quizz getQuizn() {
		return quizn;
	}

	public void setQuizn(Quizz quizn) {
		this.quizn = quizn;
	}


	

	
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	
	public TestMark() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TestMark(Long id, int sc, Quizz q, LocalDateTime dt, User u) {
		super();
		this.id = id;
		
		this.score=sc;
		this.quizn=q;
		this.date=dt;
		this.usern=u;
		
	}

	
	
	
	
	

}
