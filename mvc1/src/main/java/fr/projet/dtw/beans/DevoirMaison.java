package fr.projet.dtw.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "t_devoirMaison")
public class DevoirMaison implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 150, nullable = false)
	private String title;

	@ManyToMany(mappedBy = "devoir_maison", cascade = CascadeType.ALL)
	private List<User> students;

	@Version
	private int version;

	public DevoirMaison() {
		super();
	}

	public DevoirMaison(Long id, String title, List<User> students, int version) {
		super();
		this.id = id;
		this.title = title;
		this.students = students;
		this.version = version;
	}

	public DevoirMaison(Long id, String title, List<User> students) {
		super();
		this.id = id;
		this.title = title;
		this.students = students;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<User> getStudents() {
		return students;
	}

	public void setStudents(List<User> students) {
		this.students = students;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
