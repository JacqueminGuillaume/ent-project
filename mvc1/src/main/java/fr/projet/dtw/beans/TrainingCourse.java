package fr.projet.dtw.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="t_trainingcourse")
public class TrainingCourse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=150, nullable=false)
	private String title;
	
	@ManyToMany(mappedBy="courses", cascade=CascadeType.ALL)
	private List<User> students;	
	
	@ManyToOne(cascade=CascadeType.ALL)
	private User professor;
	
	@Column(length=150, nullable=false)
	private String level;
	
	@Version
	private int version;

	public TrainingCourse() {
		super();
	}
	
	public TrainingCourse(Long id, String title, List<User> students, User professor, String level, int version) {
		super();
		this.id = id;
		this.title = title;
		this.students = students;
		this.professor = professor;
		this.level = level;
		this.version = version;
	}
	
	public TrainingCourse(Long id, String title, List<User> students) {
		super();
		this.id = id;
		this.title = title;
		this.students = students;
	}
	
	public Long getId() {
		return id;
	} 

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<User> getStudents() {
		return students;
	}

	public void setStudents(List<User> students) {
		this.students = students;
	}

	public User getProfessor() {
		return professor;
	}

	public void setProfessor(User professor) {
		this.professor = professor;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
}
