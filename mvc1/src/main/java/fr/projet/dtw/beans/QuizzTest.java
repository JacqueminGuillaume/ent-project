package fr.projet.dtw.beans;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="t_test")
public class QuizzTest {
	
	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Quizz quiz;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private User user;
	
	
	
	@Column()
	private String question;

	@Column()
	private String reponse1;
	
	@Column()
	private String reponse2;
	
	@Column()
	private Boolean solution1;
	
	@Column()
	private Boolean solution2;
	
	
	@Column()
	private String reponse;
	
	
	@Column()
	private int etatReponse;
	
	
	
	
	
	
	

	public int getEtatReponse() {
		return etatReponse;
	}

	public void setEtatReponse(int  etatReponse) {
		this.etatReponse = etatReponse;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	//@Temporal(TemporalType.DATE)
	@Column
	private LocalDateTime createTest;
	
	private int score;
	
	@Version
	private int version;

	public QuizzTest() {
		super();
	}

	public QuizzTest(Long id, Quizz quiz, User user, String question, String reponse1, String reponse2,
			Boolean solution1, Boolean solution2, LocalDateTime createTest, int score, String reponse, int er, int version) {
		super();
		this.id = id;
		this.quiz = quiz;
		this.user = user;
		this.question = question;
		this.reponse1 = reponse1;
		this.reponse2 = reponse2;
		this.solution1 = solution1;
		this.solution2 = solution2;
		this.createTest = createTest;
		this.score = score;
		this.version = version;
		this.reponse = reponse;
		this.etatReponse = er;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Quizz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quizz quiz) {
		this.quiz = quiz;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getReponse1() {
		return reponse1;
	}

	public void setReponse1(String reponse1) {
		this.reponse1 = reponse1;
	}

	public String getReponse2() {
		return reponse2;
	}

	public void setReponse2(String reponse2) {
		this.reponse2 = reponse2;
	}

	public Boolean getSolution1() {
		return solution1;
	}

	public void setSolution1(Boolean solution1) {
		this.solution1 = solution1;
	}

	public Boolean getSolution2() {
		return solution2;
	}

	public void setSolution2(Boolean solution2) {
		this.solution2 = solution2;
	}

	public LocalDateTime getCreateTest() {
		return createTest;
	}

	public void setCreateTest(LocalDateTime dt) {
		this.createTest = dt;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	
	

	
}
