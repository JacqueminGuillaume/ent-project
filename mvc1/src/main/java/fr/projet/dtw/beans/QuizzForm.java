package fr.projet.dtw.beans;

public class QuizzForm {

	
	private String titre;
	private String niveau;
	
	
	
	public QuizzForm() {
		
	}



	public QuizzForm(String titre, String niveau) {
		super();
		this.titre = titre;
		this.niveau = niveau;
	}



	public String getTitre() {
		return titre;
	}



	public void setTitre(String titre) {
		this.titre = titre;
	}



	public String getNiveau() {
		return niveau;
	}



	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	
	
	
}
