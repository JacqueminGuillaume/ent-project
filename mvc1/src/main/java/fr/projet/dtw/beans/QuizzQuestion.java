package fr.projet.dtw.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="t_questions")
public class QuizzQuestion implements Serializable{
	
	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=150, nullable=false)
	private String title;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Quizz quizq;
	
	
	
	@OneToMany(mappedBy="question",fetch=FetchType.EAGER)
	private List<QuizzResponse> reponses;
		

	
	
	private int num;
	
	@Version
	private int version;

	public QuizzQuestion() {
		super();
	}

	public QuizzQuestion(Long id, String title, Quizz quizq, List<QuizzResponse> reponses, int num,
			int version) {
		super();
		this.id = id;
		this.title = title;
		this.quizq = quizq;
		this.reponses = reponses;
		
		this.num = num;
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Quizz getQuizq() {
		return quizq;
	}

	public void setQuizq(Quizz quizq) {
		this.quizq = quizq;
	}

	public List<QuizzResponse> getReponses() {
		return reponses;
	}

	public void setReponses(List<QuizzResponse> reponses) {
		this.reponses = reponses;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}