package fr.projet.dtw.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="t_quizzresponse")
public class QuizzResponse implements Serializable{
	
	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private QuizzQuestion question;

	@Column
	private String response;
	
	@Column
	private boolean correct;
	
	@Version
	private int version;
	
	

	public QuizzResponse() {
		super();
	
	}



	public QuizzResponse(Long id, QuizzQuestion qrep, String response, boolean correct, int version) {
		super();
		this.id = id;
		this.question = qrep;
		this.response = response;
		this.correct = correct;
		this.version = version;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public QuizzQuestion getQrep() {
		return question;
	}



	public void setQrep(QuizzQuestion qrep) {
		this.question = qrep;
		
	}



	public String getResponse() {
		return response;
	}



	public void setResponse(String response) {
		this.response = response;
	}



	public boolean isCorrect() {
		return correct;
	}



	public void setCorrect(boolean correct) {
		this.correct = correct;
	}



	public int getVersion() {
		return version;
	}



	public void setVersion(int version) {
		this.version = version;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
