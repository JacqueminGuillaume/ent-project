package fr.projet.dtw.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "t_quizz")
public class Quizz implements Serializable {

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 150, nullable = false)
	private String niveau;

	@Column(length = 150, nullable = false)
	private String domaine;
	
	
	@Column(length = 150, nullable = false)
	private String chapitre; 
	
	@Version
	private int version;
	
	

	@OneToMany(mappedBy = "quizq")
	private List<QuizzQuestion> questions;

	@OneToMany(mappedBy = "quiz", fetch = FetchType.EAGER)
	private List<QuizzTest> tests;

	@ManyToMany(mappedBy = "quiz_user", cascade = CascadeType.ALL)
	private List<User> etudiants;

	public Quizz() {
		super();
		
	}

	public Quizz(Long id, String niveau, String domaine, int version, List<QuizzTest> test, List<User> etudiants, String chap) {
		super();
		this.id = id;
		this.niveau = niveau;
		this.domaine = domaine;
		this.version = version;
		this.tests = test;
		this.etudiants = etudiants;
		this.chapitre = chap;
	}

	public Quizz(String chap, Long id, String niveau, String domaine, int version, List<QuizzQuestion> quest) {
		super();
		this.id = id;
		this.niveau = niveau;
		this.domaine = domaine;
		this.version = version;
		this.questions = quest;
		this.chapitre = chap;
	}


	public String getChapitre() {
		return chapitre;
	}

	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	public String getDomaine() {
		return domaine;
	}

	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<QuizzQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuizzQuestion> questions) {
		this.questions = questions;
	}

	public List<QuizzTest> getTests() {
		return tests;
	}

	public void setTests(List<QuizzTest> tests) {
		this.tests = tests;
	}

	public List<User> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(List<User> etudiants) {
		this.etudiants = etudiants;
	}

	@Override
	public String toString() {
		return "Quizz [chapitre=" + chapitre +", niveau=" + niveau + ", domaine=" + domaine + "]";
	}



	
	


}
