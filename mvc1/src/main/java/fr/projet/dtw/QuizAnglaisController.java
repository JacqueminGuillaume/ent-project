package fr.projet.dtw;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.QuizzTest;
import fr.projet.dtw.beans.TestMark;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.dao.QuizzDao;
import fr.projet.dtw.dao.QuizzQuestionDao;
import fr.projet.dtw.dao.QuizzResponseDao;
import fr.projet.dtw.dao.QuizzTestDao;
import fr.projet.dtw.dao.TestMarkDao;
import fr.projet.dtw.dao.UserDao;
import fr.projet.dtw.formbeans.LoginForm;
import fr.projet.dtw.formbeans.MatiereForm;
import fr.projet.dtw.formbeans.Test;
import fr.projet.dtw.formbeans.TestEleve;
import fr.projet.dtw.formbeans.TestForm;
import fr.projet.dtw.formbeans.TestFormPublic;
import fr.projet.dtw.formbeans.TestPublic;
import fr.projet.dtw.formbeans.TestQuizz;
import fr.projet.dtw.formbeans.TestSummary;


@Controller

public class QuizAnglaisController {
	
	
		@Autowired
		private UserDao userDao;

		public void setUserDao(UserDao userDao) {
			this.userDao = userDao;
		}
		
		@Autowired
		private TestMarkDao testMarkDao;
		
		
		@Autowired
		private QuizzDao quizzDao;
		
		@Autowired
		private QuizzQuestionDao questionDao;
		
		
		@Autowired
		private QuizzResponseDao reponseDao;
		
		@Autowired
		private QuizzTestDao testDao;
		
		

		
		
		
		
		
		
		
		@GetMapping("/niveau/{domaine}")
		public String espaceAnglais(Model m,HttpSession session,
				@PathVariable(name="domaine", required=false)String domaine) {
			List<String> classes = new ArrayList<>();
			classes.add("6ème");
			classes.add("5ème");
			classes.add("4ème");
			classes.add("3ème");
			classes.add("Seconde");
			classes.add("Première");
			classes.add("Terminale");
			
			session.setAttribute("domaine",  domaine);
			m.addAttribute("classes",  classes);
			TestPublic cc = new TestPublic ();
			
			m.addAttribute("cc", cc);
			
			
			List<Quizz> listeAllDomainQZ0 = quizzDao.findByDomain(domaine);
			
			List<Quizz> listeAllDomainQZ = new ArrayList<>();
			
			for (Quizz q:listeAllDomainQZ0) {
				String classe = q.getNiveau();
				classe = classe + "ème";
				Quizz qt = new Quizz(); 
				qt = q;
				qt.setNiveau(classe);
				listeAllDomainQZ.add(qt);
			}
			
			m.addAttribute("listeAllDomainQZ",  listeAllDomainQZ);
			return "quiztest";
			
		}
	
		@PostMapping( "home/test-online1/")
		public String homeTestOnline (Model m,HttpSession session,
				
				@ModelAttribute("cc") TestPublic cc, BindingResult result,
				@RequestParam(name="classe", required=false)String clas,					
									HttpServletRequest request) {
			
			
			String domaine = (String) session.getAttribute("domaine");
			String chapitre= cc.getChapitre();
			
			String classe;
			classe = clas.substring(0, 1);
			
			
			
			Quizz qz = quizzDao.findIdByAll(domaine, classe, chapitre);
			List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(qz);
			
			
			session.setAttribute("quizPublic", qz);
			session.setAttribute("chapitre", chapitre);
			int score=0, size=0;
			size=listeQuestions.size();
			
			
			session.setAttribute("numquestion",   1);
			session.setAttribute("score", score);
			
			session.setAttribute("size",  size);
			
			
			
			String quest = listeQuestions.get(0).getTitle();
			m.addAttribute("question",  quest);
			
			List<QuizzResponse>  listeReponses = listeQuestions.get(0).getReponses();
			
			String rep1 = listeReponses.get(0).getResponse();
			String rep2=  listeReponses.get(1).getResponse();
			
			Random random = new Random();
		    Boolean hasard = random.nextBoolean();
		    String reponse1, reponse2;
			if (hasard == true) {
				reponse1=rep1 ; reponse2=rep2; 
			}
			else {
				reponse1=rep2; reponse2=rep1;
			}
			
			String bonne;
			
			if (listeReponses.get(0).isCorrect()) bonne =listeReponses.get(0).getResponse();
			else
				bonne = listeReponses.get(0).getResponse();
			
			
			
			double pourcentaged = ((double) 1/ (double) size)*100;
			int pourcentage = (int) pourcentaged;
			
			
			m.addAttribute("pourcentage",  pourcentage);
			

			
			List<TestFormPublic> reponsesUtilisateur=new ArrayList<>();
			
			session.setAttribute("reponsesutilisateur", reponsesUtilisateur);
			
			
			
			m.addAttribute("chapitre", chapitre);
			m.addAttribute("question",  quest);
			m.addAttribute("reponse1",  reponse1);
			m.addAttribute("reponse2",  reponse2);
			m.addAttribute("bonne",  bonne);
			
			
			
			return "quiztest2";
		
			
}
		
//****************************************@GetMapping("test/public/control/{reponse}")
		@GetMapping("test/public/control2")
		public String testPublicControl (
								@ModelAttribute("t") Test t,
								Model m,HttpSession session,
								//	@RequestParam(name="rep1", required=false) boolean rep1,
								//	@RequestParam(name="rep2", required=false) boolean rep2,
								@PathVariable(name="reponse", required=false)String reponse,	
									
									HttpServletRequest request)
		{
			
			
			
			Quizz quizz = (Quizz) session.getAttribute("quizPublic");
			
			List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(quizz);
			
			
			String chapitre = quizz.getChapitre();
			
			int numQuestion = (int) session.getAttribute("numquestion");
			int size = (int) session.getAttribute("size");
			
			
			
			double pourcentaged = ((double) numQuestion/ (double) size)*100;
			int pourcentage = (int) pourcentaged;
			
			
			
			m.addAttribute("pourcentage",  pourcentage);
			
			
			
			List<QuizzResponse> listeReponses = listeQuestions.get(numQuestion).getReponses();
			
			
			//test solution
			String solution; String resultat;
			String solutionUser = reponse;
			int score=(int) request.getSession().getAttribute("score");
			
			
			if (listeQuestions.get(numQuestion).getReponses().get(0).isCorrect() == true) {
				solution = listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
			}
			else
			{
				solution = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			}
			
			if (solution.equals(solutionUser)) {
				
				resultat="Bonne réponse";
				score = score+1;
				request.getSession().setAttribute("score", score);
				
				
			}
			else {
				
				resultat="Mauvaise réponse";
			}
			
			
			
			List<TestFormPublic> reponsesUtilisateur = (List<TestFormPublic>) session.getAttribute("reponsesutilisateur");
			
			TestFormPublic test = new TestFormPublic();
			test.setQuestion(listeQuestions.get(numQuestion).getTitle());
			test.setReponse(solution);
			test.setUserResponse(reponse);
			reponsesUtilisateur.add(test);
			
			session.setAttribute("reponsesutilisateur", reponsesUtilisateur);
			numQuestion = numQuestion + 1;
			request.getSession().setAttribute("numquestion",  numQuestion);
			
			String bonne;
			
			if (numQuestion < listeQuestions.size())
			{
			
			
			String textQuestion = listeQuestions.get(numQuestion).getTitle();
			m.addAttribute("question",  textQuestion);
			listeReponses = listeQuestions.get(numQuestion).getReponses();
			String rep1 = listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
			String rep2 = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			Random random = new Random();
		    Boolean hasard = random.nextBoolean();
		    String reponse1, reponse2;
			if (hasard == true) {
				reponse1=rep1 ; reponse2=rep2; 
			}
			else {
				reponse1=rep2; reponse2=rep1;
			}
			
			
			
			if (listeQuestions.get(numQuestion).getReponses().get(0).isCorrect())
					bonne=listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
					
			else
				bonne = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			
			m.addAttribute("reponse1",  reponse1);
			m.addAttribute("reponse2",  reponse2);
			m.addAttribute("chapitre",  chapitre);
			m.addAttribute("bonne",  bonne);
			}
			else
			{	
			System.out.println("Fin Quizz, votre score est:"+request.getSession().getAttribute("score"));
			
				
			
			return "quiztest3";
			}
			
			
			return "quiztest2";
			
	}
			
	
			

		
		
		
		
		
		
		
		
		@GetMapping("/quizpublic/{matiere}/{niveau}")
		public String selectTest (Model m, @PathVariable(name="niveau", required=false)String niveau,
				@PathVariable(name="matiere", required=false)String matiere,
									HttpServletRequest request) {
		request.getSession().setAttribute("matiere", matiere);
		request.getSession().setAttribute("niveau",  niveau);
			List<Quizz> listeQZ = quizzDao.findById(matiere, niveau);
			for (Quizz q: listeQZ) {
				System.out.println("Quizz trouvé"+q.getId());
			}
			HttpSession session = request.getSession();
			session.setAttribute("listeQPublic", listeQZ);
			m.addAttribute("matiere",  matiere);
			m.addAttribute("niveau",  niveau);
			m.addAttribute("eleve", "guest");
			return "quiztest";
		}
		
		
		
		@GetMapping("/eleve/testquiz-online")
		public String selectTestBd (Model m, HttpSession session,
									HttpServletRequest request) {
		
			String eleve = (String) session.getAttribute("eleve");
			String classe = (String) session.getAttribute("classe");
			
			List<String> matieres = new ArrayList<>();
			matieres.add("anglais");matieres.add("maths");
			
			
			MatiereForm mf = new MatiereForm ();
			m.addAttribute("matieres",  matieres);
			m.addAttribute("mf", mf);
			
			
			List<String> listeQZ = null;
			
			listeQZ = quizzDao.findUnique();
			
			List<Quizz> listeAllQZ = null; 
			listeAllQZ = quizzDao.findAll();
			
			
			m.addAttribute("listeQZ", listeQZ);
			m.addAttribute("listeAllQZ",  listeAllQZ);
			
			User u = new User();
			m.addAttribute("u", u);
			
			return "/eleve/testonline1";
		}
		
		
	
		
		
		@GetMapping("test/public/{id}")
		public String saveTest (Model m,HttpSession session,
				@PathVariable(name="id", required=false)long id,
									
									HttpServletRequest request) {
			
			
			Quizz qz = quizzDao.findById2(id);
			List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(qz);
			
			String chapitre = qz.getChapitre();
			session.setAttribute("quizPublic", qz);
			session.setAttribute("chapitre", chapitre);
			int score=0, size=0;
			size=listeQuestions.size();
			
			
			session.setAttribute("numquestion",   1);
			session.setAttribute("score", score);
			
			session.setAttribute("size",  size);
			
			//session.setAttribute("questionsPublic", listeQuestions);
			
			String quest = listeQuestions.get(0).getTitle();
			m.addAttribute("question",  quest);
			
			List<QuizzResponse>  listeReponses = listeQuestions.get(0).getReponses();
			
			String rep1 = listeReponses.get(0).getResponse();
			String rep2=  listeReponses.get(1).getResponse();
			
			Random random = new Random();
		    Boolean hasard = random.nextBoolean();
		    String reponse1, reponse2;
			if (hasard == true) {
				reponse1=rep1 ; reponse2=rep2; 
			}
			else {
				reponse1=rep2; reponse2=rep1;
			}
			
			String bonne;
			
			if (listeReponses.get(0).isCorrect()) bonne =listeReponses.get(0).getResponse();
			else
				bonne = listeReponses.get(0).getResponse();
			
			
			
			double pourcentaged = ((double) 1/ (double) size)*100;
			int pourcentage = (int) pourcentaged;
			
			
			m.addAttribute("pourcentage",  pourcentage);
			
			List<TestFormPublic> reponsesUtilisateur=new ArrayList<>();
			
			session.setAttribute("reponsesutilisateur", reponsesUtilisateur);
			
			m.addAttribute("question", quest);
			m.addAttribute("reponse1",  reponse1);
			m.addAttribute("reponse2", reponse2);
			m.addAttribute("chapitre",  chapitre);
			m.addAttribute("bonne", bonne);
			return "quiztest2";
		}		
		
		
		
		
		//@RequestMapping(value = { "test/public/control" }, method = RequestMethod.POST)
		@GetMapping("test/public/control/{reponse}")
		public String testControl (
								//@ModelAttribute("test") TestFormPublic test,
								Model m,HttpSession session,
								//	@RequestParam(name="rep1", required=false) boolean rep1,
								//	@RequestParam(name="rep2", required=false) boolean rep2,
								@PathVariable(name="reponse", required=false)String reponse,	
									
									HttpServletRequest request)
		{
			
			
			
			Quizz quizz = (Quizz) session.getAttribute("quizPublic");
			
			List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(quizz);
			
			
			String chapitre = quizz.getChapitre();
			
			int numQuestion = (int) session.getAttribute("numquestion");
			int size = (int) session.getAttribute("size");
			
			
			
			double pourcentaged = ((double) numQuestion/ (double) size)*100;
			int pourcentage = (int) pourcentaged;
			
			
			
			m.addAttribute("pourcentage",  pourcentage);
			
			
			
			List<QuizzResponse> listeReponses = listeQuestions.get(numQuestion).getReponses();
			
			
			//test solution
			String solution; String resultat;
			String solutionUser = reponse;
			int score=(int) request.getSession().getAttribute("score");
			
			
			if (listeQuestions.get(numQuestion).getReponses().get(0).isCorrect() == true) {
				solution = listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
			}
			else
			{
				solution = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			}
			
			if (solution.equals(solutionUser)) {
				
				resultat="Bonne réponse";
				score = score+1;
				request.getSession().setAttribute("score", score);
				
				
			}
			else {
				
				resultat="Mauvaise réponse";
			}
			
			
			List<TestFormPublic> reponsesUtilisateur = (List<TestFormPublic>) session.getAttribute("reponsesutilisateur");
			
			TestFormPublic test = new TestFormPublic();
			test.setQuestion(listeQuestions.get(numQuestion).getTitle());
			test.setReponse(solution);
			test.setUserResponse(reponse);
			reponsesUtilisateur.add(test);
			
			session.setAttribute("reponsesutilisateur", reponsesUtilisateur);
			numQuestion = numQuestion + 1;
			request.getSession().setAttribute("numquestion",  numQuestion);
			
			String bonne;
			
			if (numQuestion < listeQuestions.size())
			{
			
			
			String textQuestion = listeQuestions.get(numQuestion).getTitle();
			m.addAttribute("question",  textQuestion);
			listeReponses = listeQuestions.get(numQuestion).getReponses();
			String rep1 = listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
			String rep2 = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			Random random = new Random();
		    Boolean hasard = random.nextBoolean();
		    String reponse1, reponse2;
			if (hasard == true) {
				reponse1=rep1 ; reponse2=rep2; 
			}
			else {
				reponse1=rep2; reponse2=rep1;
			}
			
			
			
			if (listeQuestions.get(numQuestion).getReponses().get(0).isCorrect())
					bonne=listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
					
			else
				bonne = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			
			m.addAttribute("reponse1",  reponse1);
			m.addAttribute("reponse2",  reponse2);
			m.addAttribute("chapitre",  chapitre);
			m.addAttribute("bonne",  bonne);
			}
			else
			{	
			System.out.println("Fin Quizz, votre score est:"+request.getSession().getAttribute("score"));
			
				
			
			return "quiztest3";
			}
			
			
			return "quiztest2";
			
	}
			
	
		
		
		
		
		
		//@GetMapping("/test/quiz/select/{domaine}/{chapitre}")
				@PostMapping( "eleve/testonline2/")
				public String eleveTestOnline (Model m,HttpSession session,
						
						@ModelAttribute("mf") MatiereForm mf, BindingResult result,
											
											HttpServletRequest request) {
					
					
					String matiere = mf.getMatiere();
					String chapitre = mf.getChapitre();
					session.setAttribute("matiere",  matiere);
					session.setAttribute("chapitre",  chapitre);
					
					
					Quizz qz = quizzDao.findByDomaineChapitre(matiere,  chapitre);
					
					
					List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(qz);
					int size = listeQuestions.size();
					
					session.setAttribute("listeQuestions", listeQuestions);
					session.setAttribute("quizztest", qz);
					session.setAttribute("numquestion",  1);
					session.setAttribute("size", size);
					
					LocalDateTime dt = LocalDateTime.now();
					session.setAttribute("date",  dt);
					
					
					
					String solution;
					List<QuizzResponse>  listeReponses = listeQuestions.get(0).getReponses();
					
					String rep1 = listeReponses.get(0).getResponse();  
					session.setAttribute("reponse1",  rep1);
					Boolean sol1=listeReponses.get(0).isCorrect();
					String rep2=  listeReponses.get(1).getResponse();  
					session.setAttribute("reponse2", rep2);
					
					
					
					Boolean sol2=listeReponses.get(1).isCorrect();
					
					if (sol1==true) solution = rep1; else solution=rep2;
					
					session.setAttribute("solution",  solution);
					
					int numquestion=1;
					double pourcentaged = ((double) numquestion/ (double) size)*100;
					int pourcentage = (int) pourcentaged;
					
					
					session.setAttribute("pourcentage",  pourcentage);
					
					String question = listeQuestions.get(0).getTitle();
					session.setAttribute("question",  question);
					
					int qstion=0;
					session.setAttribute("numquestion",  qstion);
					session.setAttribute("score",  0);
					
					
					return "eleve/testonline2";
					
					
					
					
				}
				
				
				
				
				
		
		
		@GetMapping("eleve/test-save/{reponse}")
		public String saveTest2 (Model m, HttpSession session,
				@PathVariable(name="reponse", required=false)String reponse,	
				
				HttpServletRequest request)
		{
 			
			
			int numQuestion = (int) session.getAttribute("numquestion");
			int size = (int) session.getAttribute("size");
			numQuestion= numQuestion + 1;
			session.setAttribute("numquestion",  numQuestion);
			double pourcentaged = ((double) numQuestion/ (double) size)*100;
			int pourcentage = (int) pourcentaged;
			session.setAttribute("pourcentage",  pourcentage);
			
			String resultat;
			String solution = (String) session.getAttribute("solution");
			int score=(int) request.getSession().getAttribute("score");
			Boolean sol1, sol2;
			int etatReponse;
			if (reponse.equals(solution)) {
				
				resultat="Bonne réponse";
				score = score+1;
				request.getSession().setAttribute("score", score);
				etatReponse=1;
				QuizzTest qt = new QuizzTest();
				String qst = (String) session.getAttribute("question");
				String rp1 = (String) session.getAttribute("reponse1");
				String rp2 = (String) session.getAttribute("reponse2");
				String rep = reponse;
				
		/*		if (reponse.equals(rp1)) 
				{sol1=true;sol2=false;}
				else
				{sol1=false; sol2=true;}
				qt.setSolution1(sol1);qt.setSolution2(solution2);
				*/
				
				
				LocalDateTime dt = (LocalDateTime) session.getAttribute("date");
				System.out.println("reponses before insert:"+rp1+"  "+rp2);
				qt.setCreateTest(dt);
				qt.setQuestion(qst);
				qt.setReponse1(rp1);
				qt.setReponse2(rp2);
				qt.setScore(1);
				qt.setReponse(reponse);
				qt.setEtatReponse(etatReponse);
				Long user_id = (Long) session.getAttribute("user_id");
				qt.setUser(userDao.findById(user_id));
				Quizz quizz = (Quizz) session.getAttribute("quizztest");
				qt.setQuiz(quizz);
				testDao.insert(qt);
				
				
				
				
			}
			else {
				
				resultat="Mauvaise réponse";etatReponse=0;
				QuizzTest qt = new QuizzTest();
				String qst = (String) session.getAttribute("question");
				String rp1 = (String) session.getAttribute("reponse1");
				String rp2 = (String) session.getAttribute("reponse2");
				String rep = reponse;
				LocalDateTime dt = (LocalDateTime) session.getAttribute("date");
				System.out.println("reponses before insert:"+rp1+"  "+rp2);
				qt.setCreateTest(dt);
				qt.setQuestion(qst);
				qt.setReponse1(rp1);
				qt.setReponse2(rp2);
				qt.setScore(0);
				qt.setReponse(reponse);
				qt.setEtatReponse(etatReponse);
				Long user_id = (Long) session.getAttribute("user_id");
				qt.setUser(userDao.findById(user_id));
				Quizz quizz = (Quizz) session.getAttribute("quizztest");
				qt.setQuiz(quizz);
				testDao.insert(qt);
				
				
			}
			
			
			System.out.println("question:"+numQuestion+"  "+" size:"+size);
			if (numQuestion < size)
			{
				Quizz quizz = (Quizz) session.getAttribute("quizztest");
				List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(quizz);
				List<QuizzResponse> listeReponses = listeQuestions.get(numQuestion).getReponses();
				System.out.println("on continue: numquestion:" +numQuestion);
				String textQuestion = listeQuestions.get(numQuestion).getTitle();
				session.setAttribute("question",  textQuestion);
			
				listeReponses = listeQuestions.get(numQuestion).getReponses();
				String rep1 = listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
				String rep2 = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			
			
				Random random = new Random();
				Boolean hasard = random.nextBoolean();
				String reponse1, reponse2;
				if (hasard == true) {
					reponse1=rep1 ; reponse2=rep2; 
				}
				else {
					reponse1=rep2; reponse2=rep1;
				}
				session.setAttribute("reponse1",  reponse1);
				session.setAttribute("reponse2", reponse2);
			
				String nextSolution;
			
				if (listeQuestions.get(numQuestion).getReponses().get(0).isCorrect())
					nextSolution =listeQuestions.get(numQuestion).getReponses().get(0).getResponse();
					
				else
					nextSolution = listeQuestions.get(numQuestion).getReponses().get(1).getResponse();
			
				session.setAttribute("solution",  nextSolution);
			
			
				
			}
			else
			{	
				System.out.println("fin");
				System.out.println("Fin Quizz, votre score est:"+request.getSession().getAttribute("score"));
				
				TestMark tm = new TestMark();
				
				Quizz qt = (Quizz) session.getAttribute("quizztest");
				
				
				tm.setQuizn(qt);
				
				tm.setScore(score);
				LocalDateTime dt = (LocalDateTime) session.getAttribute("date");
				tm.setDate(dt);
				Long user_id = (Long) session.getAttribute("user_id");
				tm.setUsern(userDao.findById(user_id));
				
				
				testMarkDao.insert(tm);
				quizzDao.update(qt);
				
				
			
				return "eleve/eleve";
			}
			
			
			return "eleve/testonline2";
		
		}
		
		
		
		
		//*************************DashBoard ******************
		
		
		@GetMapping("eleve/dashboard-online")
		public String dashBoard (Model m, HttpSession session,
				@PathVariable(name="reponse", required=false)String reponse,	
				
				HttpServletRequest request)
		{
			
			
			System.out.println("recherche test");
			
			Long id = (Long) session.getAttribute("user_id");
			User u = userDao.findById(id);
			//List<TestMark> userTests = testMarkDao.findByQuizzId(qz)
			System.out.println("USER :"+u.getPassword());
			List<TestMark> notes = testMarkDao.findByUser(u);
			
			List<TestEleve> testEleve = new ArrayList<>();
			
			
			for (TestMark tm: notes) {
				System.out.println("**** "+tm.getId());
				
				Quizz qz = tm.getQuizn();
				String domaine = qz.getDomaine();
				String chapitre = qz.getChapitre();
				LocalDateTime date = tm.getDate();
				int score = tm.getScore();
				TestEleve eleve = new TestEleve();
				
				//AJOUTER eleve.id
				
				eleve.setId(tm.getId());
				eleve.setChapitre(chapitre);
				eleve.setDate(date);
				eleve.setDomaine(domaine);
				eleve.setScore(score);
				
				testEleve.add(eleve);
				
				
				
			}
			
			
			session.setAttribute("notes", testEleve);
			
			//session.setAttribute("usersTests",  listeTests);
			
			System.out.println("call jsp");  
			//return "eleve/dashboard";
			
			return "eleve/dashboard";		
			
		}

		



		//**************TO COMPLETE LINKED TO ELEVE DASHBOARD 2
		@GetMapping("eleve/dashboard-online2/{id}") 
		public String dashBoard2 (Model m, HttpSession session, 
				//@RequestParam(name="date", required=false)LocalDateTime dt,
				//@ModelAttribute("t") TestSummary t,
				@PathVariable(name="id", required=false)Long id,	
				//@RequestBody LocalDateTime dt,
				HttpServletRequest request) 
		{
			
			System.out.println("id recupéré:"+id);
			Long ui = (Long) session.getAttribute("user_id");
			User u = userDao.findById(ui);
			
			System.out.println("USER :"+u.getPassword());
			TestMark test = testMarkDao.findById(id);
			
			LocalDateTime dateTest = test.getDate();
			System.out.println("date:"+dateTest);
			
			List<QuizzTest> questions = testDao.findByDate(dateTest);
			
			for (QuizzTest q: questions) {
				System.out.println("-- "+q.getScore());
			}
			
			
			m.addAttribute("questions",  questions);
		
			return "eleve/dashboard2";
		}
		
	

		public QuizzResponseDao getReponseDao() {
			return reponseDao;
		}





		public void setReponseDao(QuizzResponseDao reponseDao) {
			this.reponseDao = reponseDao;
		}
				

			

		
		
		
			
}
		
		
		

