package fr.projet.dtw;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.projet.dtw.beans.Mark;
import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.TrainingCourse;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.dao.MarkDao;
import fr.projet.dtw.dao.QuizzDao;
import fr.projet.dtw.dao.TrainingCourseDao;
import fr.projet.dtw.dao.UserDao;

@Controller
public class MarkController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
  
	@Autowired  
	private MarkDao markDao;
 	
	@Autowired
	private UserDao userDao;   
		

	
//	@GetMapping("/save-mark")
//	public String addMark(Model model) {
//		
//		//	MarkDao tc = new MarkDao(null, 10, );
//			//markDao.insert(tc);
//		//	markDao.getHibernateTemplate().evict(tc);
// 		
//	}
	
	@GetMapping("/admin/searchMarks/{id}")
	public String listNotes(Model m, HttpServletRequest req, @PathVariable("id") Long id) {
		List<Mark> notes = markDao.findMark(id);
		User user = userDao.findById(id);
		req.getSession().setAttribute("user", user);
		req.getSession().setAttribute("listeNotes", notes);
		req.getSession().setAttribute("listeVide", (notes == null || notes.size() == 0));
		req.getSession().setAttribute("showBulletin", true);
		return "redirect:/admin/users";
	}
	
}