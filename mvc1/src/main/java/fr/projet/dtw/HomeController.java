package fr.projet.dtw;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.projet.dtw.beans.User;
import fr.projet.dtw.beans.User.UserType;
import fr.projet.dtw.dao.UserDao;

@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired // car ds rott-context
	private UserDao userdao;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		return "home";
	}

	@GetMapping("/test/insert-data")
	public String insertData() {
		for (int i = 10; i < 20; i++) {
			User u = new User(null, "titi" + i, "titi" + i, "titi" + i + "@dawan.fr", UserType.PROFESSOR, null,
					"password");
			userdao.insert(u);
			userdao.getHibernateTemplate().evict(u);
		}
		return "home";
	}
}