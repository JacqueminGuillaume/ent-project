package fr.projet.dtw.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.projet.dtw.beans.User.UserType;

@WebFilter("/prof/*")
public class ProfFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();
		
		if(!UserType.PROFESSOR.equals(session.getAttribute("role"))) {
			if(session.getAttribute("isAuth") == null) {
				request.getRequestDispatcher("/authenticate").forward(request, response);
			} else {
			response.sendError(403, "Vous n'êtes pas autorisé à accéder à cette page PROF");
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	
}
