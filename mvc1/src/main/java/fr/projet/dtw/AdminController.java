package fr.projet.dtw;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.projet.dtw.beans.User;
import fr.projet.dtw.beans.User.UserType;
import fr.projet.dtw.dao.UserDao;
import fr.projet.dtw.tools.Tools;


@Controller
public class AdminController {
	@Autowired
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@GetMapping("/admin/espace-admin")
	public String admin() {
		
		return "admin/users";
	}

	@GetMapping("/admin/dashboard")
	public String showDashboard() {
		return "admin/users";
	}

	@GetMapping("/admin/users")
	public String listUsers(Model m, @RequestParam(name="page", required=false)Integer page, @RequestParam(name="max", required=false)Integer max) {
		if((page==null)) page=1;
		if(max==null) max=30;
		int start = (page-1)*max;
		List<User> lu = userDao.findAll(start,max);
		m.addAttribute("page", page); 
		m.addAttribute("max", max); 
		m.addAttribute("suivExist", (page*max)<userDao.nbUsers()); 
		
		m.addAttribute("users", lu); 
		m.addAttribute("u", new User());
		m.addAttribute("isAdd", true);
		return "admin/users";
	}

	@GetMapping("/admin/users/{action}/{id}")
	public String update(@PathVariable("action") String action, @PathVariable("id") Long id, Model model) {
		String cible = "redirect:/admin/users";
		if (action.equals("delete")) {
			userDao.remove(id);
		} else if (action.equals("update")) {
			User u = userDao.findById(id);
			model.addAttribute("u", u);
			model.addAttribute("isAdd", false);
			model.addAttribute("title", "Modification de l'utilisateur " + u.getId());
			cible = "admin/user-form";
		}
		return cible;
	}

	@PostMapping("/admin/save-user")
	public String saveUser(@ModelAttribute("u") User u, BindingResult result) {
		String cible = "redirect:/admin/users";

		if (u.getId() == null || u.getId() == 0L) {
			userDao.insert(u);
		} else {
			userDao.update(u);
		}
		return cible;
	} 

	@GetMapping("/admin/add-user")
	public String addUser(Model model) {
		model.addAttribute("u", new User());
		model.addAttribute("isAdd", true);
		model.addAttribute("title", "Ajout d'un utilisateur");
		return "admin/user-form";
	}

	@PostMapping("/admin/search-user")
	public String searchUser(Model model, @ModelAttribute("u") User u, BindingResult result) {
		List<User> lu = userDao.findByName(u.getName());
		model.addAttribute("users", lu);
		model.addAttribute("u", u);
		model.addAttribute("resVide", (lu == null || lu.size() == 0));
		
		return "admin/users";
	}

	@GetMapping("/admin/export-users")
	public void exportCsv(HttpServletResponse response) throws Exception {

		response.setContentType("text/csv");

		response.setCharacterEncoding("UTF-8");

		response.setHeader("Content-Disposition", "attachment;filename=\"users.csv\"");

		ServletOutputStream out = response.getOutputStream();

		out.write("Name;FirstName;Email;UserType".getBytes());
		out.write("\n".getBytes());

		for (User u : userDao.findAll()) {
			StringBuilder ligne = new StringBuilder();

			ligne.append(u.getName()).append(";");
			ligne.append(u.getFirstName()).append(";");
			ligne.append(u.getEmail()).append(";");
			ligne.append(u.getAccountType());
			ligne.append("\n");
			out.write(ligne.toString().getBytes());
		}
		out.close();
	}

	@PostMapping("admin/upload-users")
	public String uploadCsv(Model model, HttpServletRequest request, 
			@RequestParam("file") MultipartFile file) {
		if(!file.isEmpty()) {
			try {
				byte[] contentBytes = file.getBytes();
				String dirPath = "C:/uploads";
				File dir = new File(dirPath);
				if(!dir.exists())
					dir.mkdirs();
				String filePath = dir.getAbsolutePath()+File.separator+file.getOriginalFilename();
				try(BufferedOutputStream bos = new BufferedOutputStream(
													new FileOutputStream(filePath))) {
					bos.write(contentBytes);
				}
				
				List<User> myImportedList = Tools.importCsv(filePath);
				for (User x : myImportedList) {
					userDao.insert(x);
				}
				
				new File(filePath).delete();
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}        
		}
		return "redirect:/admin/users";
	}
	
	@GetMapping("/disconnect")
	public String disconnect(HttpServletRequest req) {
		req.getSession().invalidate();
		return "redirect:/";
	}
	
	@GetMapping("/admin/user-form")
	public String showContact(Model model) {
				model.addAttribute("typesList", UserType.values());
		
		return "contact";
	}
}