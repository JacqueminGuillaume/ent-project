package fr.projet.dtw.dao;





	import java.util.List;


	import org.hibernate.Session;
	import org.hibernate.SessionFactory;
	import org.hibernate.Transaction;
	import org.hibernate.query.Query;
	import org.springframework.orm.hibernate5.HibernateTemplate;
	import org.springframework.transaction.annotation.Transactional;

	import fr.projet.dtw.beans.Quizz;
	import fr.projet.dtw.beans.QuizzQuestion;
	import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.TestMark;
import fr.projet.dtw.beans.User;
	import fr.projet.dtw.beans.User.UserType;
	import net.sf.ehcache.hibernate.HibernateUtil;

	public class TestMarkDao {
		
		private HibernateTemplate hibernateTemplate;

		public HibernateTemplate getHibernateTemplate() {
			return hibernateTemplate;
		}

		public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
			this.hibernateTemplate = hibernateTemplate;
		}

		
		
		@Transactional
		public Long insert(TestMark q) {
			
			return (Long) hibernateTemplate.save(q);
			
			
			
		}
		
		@Transactional
		public void update(TestMark q) {
			hibernateTemplate.saveOrUpdate(q);
		}
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<TestMark> findAll() {
			return (List<TestMark>) hibernateTemplate.find("From Quizz", null);
		}
		
			
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<TestMark> findByQuizzId(Quizz qz) {
			return hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createQuery("FROM TestMark t WHERE t.quizn = :qz")
					.setParameter("qz",qz)
					.list();
		}
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<TestMark> findByUser(User u) {
			return hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createQuery("FROM TestMark t WHERE t.usern = :user")
					.setParameter("user",u)
					.list();
		}
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
		public TestMark findById(Long id){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("from TestMark where id= :id");
			
			query.setParameter("id", id);
			
			
			TestMark tmark = (TestMark) query.uniqueResult();
			
			return tmark;
			
			
		}	
		
	}
