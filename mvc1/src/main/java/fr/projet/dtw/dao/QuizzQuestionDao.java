package fr.projet.dtw.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.User;

public class QuizzQuestionDao {

		
		private HibernateTemplate hibernateTemplate;

		public HibernateTemplate getHibernateTemplate() {
			return hibernateTemplate;
		}

		public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
			this.hibernateTemplate = hibernateTemplate;
		}

		/*
		 * insertion de données dans la table t_quizz
		 */
		
		@Transactional
		public Long insert(QuizzQuestion q) {
			
			return (Long) hibernateTemplate.save(q);
		}
		
		@Transactional
		public void update(QuizzQuestion qq) {
			hibernateTemplate.saveOrUpdate(qq);
		}
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<QuizzQuestion> findAll() {
			return (List<QuizzQuestion>) hibernateTemplate.find("From QuizzQuestion", null);
		}
		
		
		@Transactional(readOnly = true)
		public List<QuizzQuestion> findById(long id) {
			return (List<QuizzQuestion>) hibernateTemplate.get(QuizzQuestion.class, id);
		}
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
		public QuizzQuestion findById2(Long id){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("from QuizzQuestion where id= :id");
			
			query.setParameter("id", id);
			
			
			QuizzQuestion question = (QuizzQuestion) query.uniqueResult();
			
			return question;
			
			
		}
		
		
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<QuizzQuestion> findByQuizzId(Quizz qz) {
			return hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createQuery("FROM QuizzQuestion qq WHERE qq.quizq = :qz")
					.setParameter("qz",qz)
					.list();
		}
		
		
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=false)
		public void saveQuestion2(String titre, String typ, Long ident){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			
			Query query = session.createQuery("INSERT INTO QuizzQuestion title,type,quizq_id) VALUES (:val1, :val2, :val3");
			
			
			query.setParameter("val1", titre);
			query.setParameter("val2", typ);
			query.setParameter("val3", ident);
			
			
			
			int result = query.executeUpdate();
			System.out.println("query done ");
			
			
			
		}
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
		public QuizzQuestion findByDomaineNiveau2(String title){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("from QuizzQuestion where title= :title");
			
			query.setParameter("title", title);
			
			
			QuizzQuestion reponse = (QuizzQuestion) query.uniqueResult();
			
			return reponse;
			
			
		}
		
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
		public List<QuizzResponse> findResponsesById(Long id){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("select reponses from QuizzQuestion where id= :id");
			
			query.setParameter("id", id);
			
			
			QuizzResponse reponse = (QuizzResponse) query.uniqueResult();
			
			return (List<QuizzResponse>) reponse;
			
			
		}
		
		
		
		
		
	}


	
	

