package fr.projet.dtw.dao;

import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.MonImage;

public class ImageDao {

	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Transactional
	public Long insert(MonImage i) {
		return (Long) hibernateTemplate.save(i);
	}

	@Transactional(readOnly = true)
	public MonImage findById(long id) {
		return hibernateTemplate.get(MonImage.class, id);
	}

}