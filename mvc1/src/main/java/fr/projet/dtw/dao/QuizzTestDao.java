package fr.projet.dtw.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.QuizzTest;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.formbeans.TestForm;

public class QuizzTestDao {
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	/*
	 * insertion de données dans la table t_quizz
	 */
	
	@Transactional
	public Long insert(QuizzTest q) {
		
		return (Long) hibernateTemplate.save(q);
		
		
	
		
	}
	
	@Transactional
	public void update(QuizzTest q) {
		hibernateTemplate.saveOrUpdate(q);
	}
	
	
	
	
	/*
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<QuizzQuestion> findAllQuestion(Long id) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM QuizzTest q WHERE q.id= :id")
				.setParameter("id", id)
				.list();  
	}
	*/
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<QuizzTest> findAll2() {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM QuizzTest")
				.list();  
	}
	
	

	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public QuizzTest findById(Long id){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from QuizzTest t join t.quizz where id= :id");
		
		query.setParameter("id", id);
		
		
		QuizzTest quizzt = (QuizzTest) query.uniqueResult();
		
		return quizzt;
		
		
	}	
	
	

	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<QuizzTest> findByUid(User u) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM QuizzTest qt WHERE qt.user = :id")
				.setParameter("id",u)
				.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<QuizzTest> findByDate(LocalDateTime dt) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM QuizzTest qt WHERE qt.createTest = :createTest")
				.setParameter("createTest",dt)
				.list();
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<LocalDateTime> findUnique(User id) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("select distinct q.createTest FROM QuizzTest q where q.user = :id")
				.setParameter("id",id)
				.list();
	}
	
	
	//Not used yet
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<QuizzTest> findQuizzUnique(User id) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM QuizzTest q where q.user = :id")
				.setParameter("id",id)
				.list();
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<QuizzTest> findBydate(LocalDateTime dt) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM QuizzTest q where q.createTest = :dt")
				.setParameter("dt",dt)
				.list();
	}
	
	
	
	
	
	
}