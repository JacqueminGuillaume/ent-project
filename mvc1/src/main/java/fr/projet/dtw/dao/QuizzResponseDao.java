package fr.projet.dtw.dao;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.User;

public class QuizzResponseDao {

		
		private HibernateTemplate hibernateTemplate;

		public HibernateTemplate getHibernateTemplate() {
			return hibernateTemplate;
		}

		public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
			this.hibernateTemplate = hibernateTemplate;
		}

		/*
		 * insertion de données dans la table t_quizz
		 */
		
		@Transactional
		public Long insert(QuizzResponse q) {
			
			return (Long) hibernateTemplate.save(q);
		}
		
		@Transactional
		public void update(QuizzResponse qq) {
			hibernateTemplate.saveOrUpdate(qq);
		}
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<QuizzResponse> findAll() {
			return (List<QuizzResponse>) hibernateTemplate.find("From QuizzResponse", null);
		}
		
		
		@Transactional(readOnly = true)
		public List<QuizzResponse> findById(long id) {
			return (List<QuizzResponse>) hibernateTemplate.get(QuizzResponse.class, id);
		}
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
		public QuizzResponse findById2(Long id){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("from QuizzResponse where id= :id");
			
			query.setParameter("id", id);
			
			
			QuizzResponse reponse = (QuizzResponse) query.uniqueResult();
			
			return reponse;
			
			
		}
		
		
		
		
		/*
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<QuizzResponse> findByQuizzId(Quizz qz) {
			return hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createQuery("FROM QuizzResponse qq WHERE qq.quizq = :qz")
					.setParameter("qz",qz)
					.list();
		}
		*/
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<QuizzResponse> findByQuestion(QuizzQuestion qs) {
			return hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createQuery("FROM QuizzResponse qq WHERE qq.question = :qs")
					.setParameter("qs",qs)
					.list();
		}
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=false)
		public void saveReponse(String reponse, boolean correct, Long ident){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			
			Query query = session.createQuery("INSERT INTO QuizzResponse response,correct,) VALUES (:val1, :val2, :val3");
			
			
			query.setParameter("val1", reponse);
			query.setParameter("val2", correct);
			query.setParameter("val3", ident);
			
			
			
			int result = query.executeUpdate();
			System.out.println("query done ");
			
			
			
		}
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
		public QuizzResponse findByDomaineNiveau2(String title){
			SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
			
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("from QuizzResponse where title= :title ");
			
			query.setParameter("title", title);
			
			
			QuizzResponse reponse = (QuizzResponse) query.uniqueResult();
			
			return reponse;
			
			
		}
		
		
		
		
		
		@SuppressWarnings("unchecked")
		@Transactional(readOnly = true)
		public List<QuizzResponse> findReponses(Long id) {
			return hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createQuery("FROM QuizzResponse qr WHERE qr.fk_question = :id")
					.setParameter("id",id)
					.list();
		}
		
		
		
		
	}


	
	

