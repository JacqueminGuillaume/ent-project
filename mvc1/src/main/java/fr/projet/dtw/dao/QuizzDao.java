package fr.projet.dtw.dao;



import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.beans.User.UserType;
import net.sf.ehcache.hibernate.HibernateUtil;

public class QuizzDao {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	
	
	@Transactional
	public Long insert(Quizz q) {
		
		return (Long) hibernateTemplate.save(q);
				
	}
	
	@Transactional
	public Long insertUser(User u) {
		
		return (Long) hibernateTemplate.save(u);
	
	}
	
	
	
	@Transactional
	public void update(Quizz q) {
		hibernateTemplate.saveOrUpdate(q);
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Quizz> findAll() {
		return (List<Quizz>) hibernateTemplate.find("From Quizz", null);
	}
	
		
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Quizz findByDomaineNiveau2(String domaine, String niveau){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Quizz where niveau= :niveau  AND domaine= :domaine");
		
		query.setParameter("niveau", niveau);
		query.setParameter("domaine",  domaine);
		
		Quizz quizz = (Quizz) query.uniqueResult();
		
		return quizz;
				
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Quizz findByDomaineChapitre(String domaine, String chapitre){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Quizz where chapitre= :chapitre  AND domaine= :domaine");
		
		query.setParameter("chapitre", chapitre);
		query.setParameter("domaine",  domaine);
		
		Quizz quizz = (Quizz) query.uniqueResult();
		
		return quizz;
				
	}
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Quizz findById2(Long id){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Quizz where id= :id");
		
		query.setParameter("id", id);
		
		
		Quizz quizz = (Quizz) query.uniqueResult();
		
		return quizz;
		
		
	}	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Quizz findIdByAll(String domaine, String niveau, String chapitre){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Quizz where niveau= :niveau AND domaine= :domaine "
				+ "AND chapitre= :chapitre");
		
		query.setParameter("niveau", niveau);
		query.setParameter("domaine", domaine);
		query.setParameter("chapitre", chapitre);
		
		Quizz quizz = (Quizz) query.uniqueResult();
		
		return quizz;
		
		
	}	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Quizz> findById(String domaine, String niveau) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM Quizz qq WHERE qq.niveau = :niveau  AND qq.domaine = :domaine")
				.setParameter("niveau",niveau)
				.setParameter("domaine", domaine)
				.list();
	}
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Quizz> findByDomain(String domaine) {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM Quizz qq WHERE qq.domaine = :domaine")
				.setParameter("domaine", domaine)
				.list();
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<String> findUnique() {
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("select distinct q.domaine FROM Quizz q")
				.list();
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Long findId(String niveau, String domaine, String chapitre){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select id from Quizz "
				+ "where niveau= :niveau  AND domaine= :domaine AND chapitre= :chapitre");
		
		query.setParameter("niveau", niveau);
		query.setParameter("domaine", domaine);
		query.setParameter("chapitre", chapitre);
		
		Long id = (Long) query.uniqueResult();
		
		return (Long) id;
		
		
	}
	

	
	

	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Quizz findByDomaine(String domaine){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Quizz where domaine= :domaine");
		
		query.setParameter("domaine",  domaine);
		
		Quizz quizz = (Quizz) query.uniqueResult();
		
		return quizz;
				
	}
	
	
	
	
	
}
