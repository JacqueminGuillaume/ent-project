package fr.projet.dtw.dao;


import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import fr.projet.dtw.beans.Mark;

public class MarkDao {
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	
	@Transactional
	public Integer insert(Mark m) {
		
		return (Integer) hibernateTemplate.save(m);
		
		
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<Mark> findMark(Long id) {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM Mark m WHERE m.student.id LIKE :rech")
				.setParameter("rech", id)
				.list();
	}
}