package fr.projet.dtw.dao;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.TrainingCourse;


public class TrainingCourseDao {
	
	private HibernateTemplate hibernateTemplate;


	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Transactional
	public Long insert(TrainingCourse t) {
	return (Long) hibernateTemplate
			.save(t);
	}
	
	@Transactional(readOnly = true)
	public TrainingCourse findById(long id) {
		return hibernateTemplate.get(TrainingCourse.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TrainingCourse> getTrainingCourse() {	
	return (List<TrainingCourse>) hibernateTemplate
			.getSessionFactory()
			.getCurrentSession()
			.createQuery("FROM TrainingCourse");
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TrainingCourse> findByUser(Long Id) {	
	return (List<TrainingCourse>) hibernateTemplate
			.getSessionFactory()
			.getCurrentSession()
			.createQuery("SELECT t FROM TrainingCourse t JOIN t.students m WHERE m.id = :id").setParameter("id", Id).list();
	}
		
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true)
	public List<TrainingCourse> findAll() {
		return (List<TrainingCourse>) hibernateTemplate.find("From TrainingCourse", null);
	}
	
}