package fr.projet.dtw.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.beans.User.UserType;

public class UserDao {
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) hibernateTemplate.find("From User", null);
	}
	
	@SuppressWarnings("unchecked") 
	@Transactional(readOnly = true)
	public List<User> findAll(int start, int nb) {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM User")
				.setFirstResult(start)
				.setMaxResults(nb).list();
	}
	
	@SuppressWarnings("deprecation")
	@Transactional(readOnly=true)
	public long nbUsers() {
		return (Long) hibernateTemplate.find("SELECT COUNT (u.id) FROM User u", null).get(0);
	}  

	@Transactional
	public Long insert(User u) {
		return (Long) hibernateTemplate.save(u);
	} 
	
	@Transactional
	public Long insertQuizz(Quizz quizz) {
		return (Long) hibernateTemplate.save(quizz);
	} 
	
	

	@Transactional(readOnly = true)
	public User findById(long id) {
		return hibernateTemplate.get(User.class, id);
	}

	@Transactional
	public void update(User u) {
		hibernateTemplate.saveOrUpdate(u);
	}

	@Transactional
	public void remove(Long id) {
		hibernateTemplate.delete(findById(id));
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findByName(String rech) {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM User u WHERE u.name LIKE :rech")
				.setParameter("rech", "%" + rech + "%")
				.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public User findByEmail(String email){
		List<User> lu = hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM User u WHERE u.email= :email")
				.setParameter("email", email)
				.list();  
		if(lu!=null && lu.size()>0) {
			return lu.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findByNameAccountType(String rech) {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM User u WHERE u.name LIKE :rech AND u.accountType= :type ")
				.setParameter("rech", "%" + rech + "%")
				.setParameter("type", UserType.STUDENT)
				.list();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findByNameProfessor(String rech) {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM User u WHERE u.name LIKE :rech AND u.accountType= :type ")
				.setParameter("rech", "%" + rech + "%")
				.setParameter("type", UserType.PROFESSOR)
				.list();
	}
	
	
	
	//Added by Mohamed
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public User findUser(Long id){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from User where id= :id");
		
		query.setParameter("id", id);
		
		
		User reponse = (User) query.uniqueResult();
		
		return reponse;
		
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findProfessorAbsent() {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM User u WHERE u.accountType= :type AND u.absent= true")
				.setParameter("type", UserType.PROFESSOR)
				.list();
	}
}