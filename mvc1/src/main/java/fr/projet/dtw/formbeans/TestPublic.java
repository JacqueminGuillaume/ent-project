package fr.projet.dtw.formbeans;

public class TestPublic {
	
	private String classe;
	private String chapitre;
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getChapitre() {
		return chapitre;
	}
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}
	public TestPublic() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TestPublic(String classe, String chapitre) {
		super();
		this.classe = classe;
		this.chapitre = chapitre;
	}
	
	

}
