package fr.projet.dtw.formbeans;

public class QuizzForm {
	
	private String chapitre;
	private String niveau;
	private String domaine;
	private String title;
	private String reponse;
	private String correct;
	
	
	public String getChapitre() {
		return chapitre;
	}
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}
	public String getNiveau() {
		return niveau;
	}
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getReponse() {
		return reponse;
	}
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	public String getCorrect() {
		return correct;
	}
	public void setCorrect(String correct) {
		this.correct = correct;
	}
	public QuizzForm(String niveau, String domaine, String title, String reponse, String correct, String chapitre) {
		super();
		this.niveau = niveau;
		this.domaine = domaine;
		this.title = title;
		this.reponse = reponse;
		this.correct = correct;
		this.chapitre = chapitre;
	}
	public QuizzForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
