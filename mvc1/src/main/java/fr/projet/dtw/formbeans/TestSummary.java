package fr.projet.dtw.formbeans;

import java.time.LocalDateTime;

public class TestSummary {
	
	private LocalDateTime date;
	private String domaine;
	private String chapitre;
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getChapitre() {
		return chapitre;
	}
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}
	public TestSummary() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TestSummary(LocalDateTime date, String domaine, String chapitre) {
		super();
		this.date = date;
		this.domaine = domaine;
		this.chapitre = chapitre;
	}
	
	
	

}
