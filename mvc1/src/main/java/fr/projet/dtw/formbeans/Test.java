package fr.projet.dtw.formbeans;

public class Test {
	
	
	
	private String firstname;
	private String lastname;
	private String question;
	private String reponse1;
	private String reponse2;
	private String chapitre;
	private String bonne;
	
	
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getReponse1() {
		return reponse1;
	}
	public void setReponse1(String reponse1) {
		this.reponse1 = reponse1;
	}
	public String getReponse2() {
		return reponse2;
	}
	public void setReponse2(String reponse2) {
		this.reponse2 = reponse2;
	}
	public String getChapitre() {
		return chapitre;
	}
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}
	public String getBonne() {
		return bonne;
	}
	public void setBonne(String bonne) {
		this.bonne = bonne;
	}
	public Test() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Test(String question, String reponse1, String reponse2, String chapitre, String bonne, String fn, String ln) {
		super();
		this.question = question;
		this.reponse1 = reponse1;
		this.reponse2 = reponse2;
		this.chapitre = chapitre;
		this.bonne = bonne;
		this.firstname=fn;
		this.lastname=ln;
	}
	
	
	

}
