package fr.projet.dtw.formbeans;

import java.time.LocalDateTime;

import fr.projet.dtw.beans.Quizz;

public class TestQuizz {
	
	private Quizz quiz;
	private LocalDateTime dateTest;
	public Quizz getQuiz() {
		return quiz;
	}
	public void setQuiz(Quizz quiz) {
		this.quiz = quiz;
	}
	public LocalDateTime getDateTest() {
		return dateTest;
	}
	public void setDateTest(LocalDateTime dateTest) {
		this.dateTest = dateTest;
	}
	public TestQuizz() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TestQuizz(Quizz quiz, LocalDateTime dateTest) {
		super();
		this.quiz = quiz;
		this.dateTest = dateTest;
	}
	

}
