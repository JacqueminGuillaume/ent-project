package fr.projet.dtw.formbeans;

import java.time.LocalDateTime;

public class TestForm {
	
	private Long id;
	private String question;
	private String reponse1;
	private String reponse2;
	private boolean rep1;
	private boolean rep2;
	
	
	
	
	public TestForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TestForm(Long id, String question, String reponse1, boolean rep1, String reponse2, boolean rep2) {
		super();
		this.id = id;
		this.question = question;
		this.reponse1 = reponse1;
		this.rep1 = rep1;
		this.reponse2 = reponse2;
		this.rep2 = rep2;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getReponse1() {
		return reponse1;
	}
	public void setReponse1(String reponse1) {
		this.reponse1 = reponse1;
	}
	public boolean isRep1() {
		return rep1;
	}
	public void setRep1(boolean rep1) {
		this.rep1 = rep1;
	}
	public String getReponse2() {
		return reponse2;
	}
	public void setReponse2(String reponse2) {
		this.reponse2 = reponse2;
	}
	public boolean isRep2() {
		return rep2;
	}
	public void setRep2(boolean rep2) {
		this.rep2 = rep2;
	}
	
	
	
	

}
