package fr.projet.dtw.formbeans;

import java.time.LocalDateTime;

public class TestEleve {
	
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private String domaine;
	private String chapitre;
	private int score;
	private LocalDateTime date;
	
	
	
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getChapitre() {
		return chapitre;
	}
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public TestEleve() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public TestEleve(String domaine, String chapitre, int score, LocalDateTime date, Long i) {
		super();
		this.domaine = domaine;
		this.chapitre = chapitre;
		this.score = score;
		this.date = date;
		this.id=i;
		
	}
	

}
