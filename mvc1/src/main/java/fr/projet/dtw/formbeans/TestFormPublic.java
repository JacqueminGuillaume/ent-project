package fr.projet.dtw.formbeans;

public class TestFormPublic {
	
	
	private String question;
	private String userResponse;
	private String reponse;
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getUserResponse() {
		return userResponse;
	}
	public void setUserResponse(String userResponse) {
		this.userResponse = userResponse;
	}
	public String getReponse() {
		return reponse;
	}
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	public TestFormPublic() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TestFormPublic(String question, String userResponse, String reponse) {
		super();
		this.question = question;
		this.userResponse = userResponse;
		this.reponse = reponse;
	}
	
	
	
	

}
