package fr.projet.dtw;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ParentController {
	
	@GetMapping("/espace-parent")
	public String admin() {
		
		return "parent/parent";
	}

}
