package fr.projet.dtw;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.projet.dtw.beans.Mark;
import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.dao.MarkDao;
import fr.projet.dtw.dao.QuizzDao;
import fr.projet.dtw.dao.QuizzQuestionDao;
import fr.projet.dtw.dao.QuizzResponseDao;
import fr.projet.dtw.dao.QuizzTestDao;
import fr.projet.dtw.dao.UserDao;

@Controller
public class ProfController {
	
	@Autowired
	private UserDao userdao;
	
	@Autowired
	private QuizzDao quizzdao;
	
	@Autowired
	private QuizzQuestionDao questionDao;
	
	
	@Autowired
	private QuizzResponseDao reponseDao;
	
	@Autowired
	private QuizzTestDao testDao;
	
	@Autowired
	private MarkDao markdao;
	
	public void setUserdao(UserDao userdao) {
		this.userdao = userdao;
	}
	
	public void setQuizzdao(QuizzDao quizzdao) {
		this.quizzdao = quizzdao;
	}

	@GetMapping("/prof/espace-prof")
	public String admin(Model m) {
		
		m.addAttribute("u", new User());		
		m.addAttribute("initialise", true);		
		m.addAttribute("quizz", new Quizz());
		return "prof/prof";
	}
		
	@PostMapping("/prof/search-user")
	public String searchUser(Model model, @ModelAttribute("u") User u, BindingResult result) {
		List<User> lu = userdao.findByNameAccountType(u.getName());
		model.addAttribute("users", lu);
		model.addAttribute("u", u);
		model.addAttribute("initialise", false);
		model.addAttribute("resVide", (lu == null  || lu.size() == 0) );
		model.addAttribute("searchUser", true);
		return "prof/prof";
	}
	

	
	
	@PostMapping("/prof/search-quizz")
	public String searchAllQuizz(Model model, @ModelAttribute("q") Quizz q, 
			BindingResult result) {
		List<Quizz> listQuizz = quizzdao.findAll();
		model.addAttribute("quizzForm", new Quizz());
		model.addAttribute("quizz", listQuizz);
		model.addAttribute("q", q);
		model.addAttribute("resVide", (listQuizz == null  || listQuizz.size() == 0) );

		return "prof/attrQuizz";
		
	}
	
	@PostMapping("/prof/save-quizz")
	public String addQuizz(
			@ModelAttribute("q") Quizz q, Model m, 
			@RequestParam(name="id", required=false) Long id,
			HttpServletRequest request,
			BindingResult result
			) {
		
		Quizz quizz = new Quizz();
		User u = new User();		
		quizz.setDomaine(q.getDomaine());
		quizz.setNiveau(q.getNiveau());
		quizz.setChapitre(q.getChapitre());
		quizzdao.insert(quizz);
		m.addAttribute("u", u);
		m.addAttribute("quizz", quizz);
		m.addAttribute("ajoutQuestResp", true);
		
		
		return "prof/prof";
	}
	
	@GetMapping("/prof/attrNotes")
	public String addNotes (Model model, HttpServletRequest req) {
		long id = Long.parseLong(req.getParameter("id"));
		User user = userdao.findById(id);
		model.addAttribute("u", new User());
		model.addAttribute("user", user);
		model.addAttribute("mark", new Mark());
		model.addAttribute("addNotes", true);
		return "prof/prof";
		
	}
	
	@PostMapping("/prof/sendMark")
	public String sendMarkInDb(Model model, @ModelAttribute("mark") Mark mark,
			BindingResult result, HttpServletRequest req) {
		long id_eleve = Long.parseLong(req.getParameter("id_eleve"));
		User user = userdao.findById(id_eleve);
		
		Mark m = new Mark();
		m.setValue(mark.getValue());
		m.setStudent(user);
		model.addAttribute("u", new User());
		model.addAttribute("question", new QuizzQuestion());
		model.addAttribute("mark", new Mark());
		
		markdao.insert(m);

		return "prof/prof";
	}
	
	@GetMapping("prof/quizz/add")
	public String addQuizz1 (Model m, 
								
								HttpServletRequest request) {
		
		
		m.addAttribute("u", new User());
		m.addAttribute("quizz", new Quizz());
		m.addAttribute("addQuizzRequest", true);
		
		return "prof/prof";
		
	}
	
	@GetMapping("prof/question/add")
	public String addQuestion (Model model, HttpServletRequest req) {
		List<Quizz> listeQuizz = quizzdao.findAll();
		model.addAttribute("listeQuizz", listeQuizz);
		model.addAttribute("u", new User());
		model.addAttribute("question", new QuizzQuestion());
		model.addAttribute("ajoutQuestResp", true);
		return "prof/prof";
	}
	
	@GetMapping("prof/question/save")
	public String addReponse ( Model model, HttpServletRequest req) {
		long id = Long.parseLong(req.getParameter("id"));
		Quizz quizz = quizzdao.findById2(id);
		List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(quizz);
		model.addAttribute("listeQuestions", listeQuestions);
		model.addAttribute("u", new User());
		model.addAttribute("quizztrouver", quizz);
		model.addAttribute("quizzquestion", new QuizzQuestion());
		model.addAttribute("ajoutReponse", true);
		
		return "prof/prof";
		
	}
	
	@PostMapping("prof/question/saveInDb")
	public String AddReponseInDb (Model model, @ModelAttribute("quizzquestion") QuizzQuestion quizzquestion,
			BindingResult result, HttpServletRequest req) {
		long id_quizz = Long.parseLong(req.getParameter("id_quizz"));
		Quizz quizz = quizzdao.findById2(id_quizz);
		
		QuizzQuestion qq = new QuizzQuestion();
		qq.setTitle(quizzquestion.getTitle());		
		qq.setQuizq(quizz);
		
		questionDao.insert(qq);
		List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(quizz);
			
		model.addAttribute("u", new User());
		model.addAttribute("listeQuestions", listeQuestions);
		model.addAttribute("quizztrouver", quizz);
		model.addAttribute("quizzquestion", new QuizzQuestion());
		model.addAttribute("reponse", new QuizzResponse());
		model.addAttribute("showReponse", true);
		model.addAttribute("ajoutReponse", true);
		
		return "prof/prof";
	}
	
	@GetMapping("prof/reponse/add")
	public String saveReponse (Model model, HttpServletRequest req) {
		long id_quest = Long.parseLong(req.getParameter("id"));
		System.out.println("idQuest" + id_quest);
		QuizzQuestion question = questionDao.findById2(id_quest);
		//List<QuizzQuestion> listeQuestions = questionDao.findByQuizzId(quizz);
		model.addAttribute("reponse", new QuizzResponse());
		model.addAttribute("question", question);
		model.addAttribute("u", new User());
		model.addAttribute("writeResponse", true);
		
		
		req.getSession().setAttribute("question", question);
		
		
		return "prof/prof";
		
	}
	
	@PostMapping("prof/response/save-response")
	public String saveReponseInDb (Model model, @ModelAttribute("reponse") QuizzResponse quizzresponse, BindingResult result, HttpServletRequest req) {
		long id_question = Long.parseLong(req.getParameter("id_question"));
		
		QuizzQuestion question = questionDao.findById2(id_question);
		QuizzResponse reponse = new QuizzResponse();
		
		reponse.setResponse(quizzresponse.getResponse());
		reponse.setCorrect(quizzresponse.isCorrect());
		reponse.setQrep(question);
		
		reponseDao.insert(reponse);
		
		model.addAttribute("u", new User());
		model.addAttribute("quizzquestion", new QuizzQuestion());
		model.addAttribute("question", question);
		
		req.getSession().setAttribute("question", question);
		
		return "redirect:/prof/reponse/add?id="+id_question;
	}
	
	@GetMapping("prof/attrQuizz")
	public String attrQuizz(Model model, HttpServletRequest req) {
		long idUser = Long.parseLong(req.getParameter("id"));
		User user = userdao.findById(idUser);
		model.addAttribute("u", new User());
		model.addAttribute("showAttrQuizz", true);
		model.addAttribute("user", user);
		model.addAttribute("quizz", new Quizz());
		List<Quizz> listeQuizz = quizzdao.findAll();
		model.addAttribute("listeQuizz", listeQuizz);
		
		req.getSession().setAttribute("user", user);
		
		return "prof/prof";
	}
	
	@PostMapping("prof/AttrQuizzEleve")
	public String attrQuizzEleve(Model model, @ModelAttribute("quizz") Quizz quizz, BindingResult result, HttpServletRequest req ) {
		List<Quizz> listeQuizz = quizzdao.findAll();
		
		long idUser = Long.parseLong(req.getParameter("idUser"));
		User user = userdao.findById(idUser);
		
		Quizz quizzTrouver = quizzdao.findByDomaine(quizz.getDomaine());
		List<Quizz> listeQuizzEleve = new ArrayList<>();
		listeQuizzEleve.add(quizzTrouver);
		
		user.setQuiz_user(listeQuizzEleve);
		
		userdao.update(user);
		
		model.addAttribute("u", new User());
		model.addAttribute("user", user);
		model.addAttribute("quizz", new Quizz());
		model.addAttribute("listeQuizz", listeQuizz);
		return "prof/prof";
	}
	
	
	
	
}