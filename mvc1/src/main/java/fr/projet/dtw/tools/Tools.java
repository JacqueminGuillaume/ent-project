package fr.projet.dtw.tools;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import fr.projet.dtw.beans.Quizz;
import fr.projet.dtw.beans.QuizzQuestion;
import fr.projet.dtw.beans.QuizzResponse;
import fr.projet.dtw.beans.User;
import fr.projet.dtw.beans.User.UserType;
import fr.projet.dtw.formbeans.QuizzForm;

public class Tools {

	public static <T> void toCsv(String filePath, List<T> myList, List<String> columns, String separator) throws Exception {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
			if (myList != null && myList.size() > 0) {
				Field[] tab = myList.get(0).getClass().getDeclaredFields();
				StringBuilder ligneEntete = new StringBuilder();
				for (Field f : tab) {
					f.setAccessible(true);
					if (columns.contains(f.getName()))
						ligneEntete.append(f.getName()).append(separator);
				}
				bw.write(ligneEntete.toString().substring(0, ligneEntete.length() - 1));
				bw.newLine();
				for(T obj : myList) {
					StringBuilder line = new StringBuilder();
					for (Field f : tab) {
						if(columns.contains(f.getName()))
							line.append(f.get(obj).toString()).append(separator);
					}
					bw.write(line.toString().substring(0, line.length() - 1));
					bw.newLine();
				}
			}
		}
	}



	public static List<User> importCsv(String filePath) throws Exception {
		List<User> users= new ArrayList<>();
		try(BufferedReader reader = new BufferedReader(new FileReader(filePath))){
			reader.readLine();
			String ligne = null;
			while((ligne=reader.readLine())!=null) {
				if(!ligne.trim().isEmpty()) {

					String[] tab = ligne.split(";");

					if(tab.length == 4) {
						try {
							User u = new User();
							u.setName(tab[0]);
							u.setFirstName(tab[1]);
							u.setEmail(tab[2]);
							u.setAccountType(UserType.valueOf(tab[3]));

							users.add(u);
						} catch(Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return users;
	} 
	
	
	
	public static List<QuizzForm> importQuizzCsv(String filePath) throws Exception {
		List<QuizzForm> quizzs= new ArrayList<>();
		
		
		try(BufferedReader reader = new BufferedReader(new FileReader(filePath))){
			reader.readLine();
			String ligne = null;
			
			while((ligne=reader.readLine())!=null) {
				if(!ligne.trim().isEmpty()) {

					String[] tab = ligne.split(";");
					
						try {
							QuizzForm q= new QuizzForm();
							q.setNiveau(tab[0]);
							
							q.setDomaine(tab[1]);
							q.setChapitre(tab[2]);
							q.setTitle(tab[3]);
							q.setCorrect(tab[4]);
							q.setReponse(tab[5]);
							
							
							quizzs.add(q);
						} catch(Exception ex) {
							ex.printStackTrace();
						
					}
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return quizzs;
	} 
	
	
	
	
}